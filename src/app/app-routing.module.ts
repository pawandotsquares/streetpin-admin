import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './auth/auth.guard';
import { LoginComponent } from './component/login/login.component';
import { ForgotpasswordComponent } from './component/forgotpassword/forgotpassword.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { ResetpasswordComponent } from './component/resetpassword/resetpassword.component';
import { ChangepasswordComponent } from './component/changepassword/changepassword.component';
import { PagenotfoundComponent } from './component/pagenotfound/pagenotfound.component';
import { UsersComponent } from './component/users/users.component';
import { UserDetailsComponent } from './component/user-details/user-details.component';
import { DiscountCodeComponent } from './component/discount-code/discount-code.component';
import { StaticPagesComponent } from './component/static-pages/static-pages.component';
import { StaticPagesEditComponent } from './component/static-pages-edit/static-pages-edit.component';
import { CmsComponent } from './component/cms/cms.component';
import { PinboardComponent } from './component/pinboard/pinboard.component';
import { OffersComponent } from './component/offers/offers.component';
import { TownpinboardComponent } from './component/townpinboard/townpinboard.component';
import { SubscriptionsComponent } from './component/subscriptions/subscriptions.component';
import { CreditPlanComponent } from './component/credit-plan/credit-plan.component';
import { PlanComponent } from './component/plan/plan.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'changepassword', component: ChangepasswordComponent, canActivate: [AuthGuard] },
  { path: 'cms', component: CmsComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'discountvouchers', component: DiscountCodeComponent, canActivate: [AuthGuard] },
  { path: 'forgotpassword', component: ForgotpasswordComponent },
  { path: 'offers', component: OffersComponent, canActivate: [AuthGuard] },
  { path: 'pages', component: StaticPagesComponent, canActivate: [AuthGuard] },
  { path: 'pinboard', component: PinboardComponent, canActivate: [AuthGuard] },
  { path: 'page-edit/:id', component: StaticPagesEditComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
  { path: 'users-details/:id', component: UserDetailsComponent, canActivate: [AuthGuard] },
  { path: 'resetpassword/:id', component: ResetpasswordComponent },
  { path: 'subscriptions', component: SubscriptionsComponent, canActivate: [AuthGuard] },
  { path: 'subscriptions-plan', component: PlanComponent, canActivate: [AuthGuard] },
  { path: 'my-offer-credits', component: CreditPlanComponent, canActivate: [AuthGuard] },
  { path: 'town-pinboard', component: TownpinboardComponent, canActivate: [AuthGuard] },
  { path: 'notfound', component: PagenotfoundComponent },
  { path: '', component: LoginComponent },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];
@NgModule({
  imports: [
    [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
