import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { ChangepasswordComponent } from './component/changepassword/changepassword.component';
import { ForgotpasswordComponent } from './component/forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './component/resetpassword/resetpassword.component';
import { LeftSidebarComponent } from './component/left-sidebar/left-sidebar.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { PagenotfoundComponent } from './component/pagenotfound/pagenotfound.component';
import { UsersComponent } from './component/users/users.component';
import { UserDetailsComponent } from './component/user-details/user-details.component';
import { DiscountCodeComponent } from './component/discount-code/discount-code.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { StaticPagesComponent } from './component/static-pages/static-pages.component';
import { StaticPagesEditComponent } from './component/static-pages-edit/static-pages-edit.component';
import { NgxEditorModule } from 'ngx-editor';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditorModule } from 'primeng/editor';
import { CalendarModule } from 'primeng/calendar';
import { CmsComponent } from './component/cms/cms.component';
import { PinboardComponent } from './component/pinboard/pinboard.component';
import { TownpinboardComponent } from './component/townpinboard/townpinboard.component';
import { ToastModule } from 'primeng/toast';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CreditPlanComponent } from './component/credit-plan/credit-plan.component';
import { SubscriptionsComponent } from './component/subscriptions/subscriptions.component';
import { OffersComponent } from './component/offers/offers.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxTrimDirectiveModule } from 'ngx-trim-directive';
import { CheckboxModule } from 'primeng/checkbox';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { ColorPickerModule } from 'ngx-color-picker';
import { DropdownModule } from 'primeng/dropdown';
import { NgxPaginationModule } from 'ngx-pagination';
import { MultiSelectModule } from 'primeng/multiselect';
import { PlanComponent } from './component/plan/plan.component';
import { HttpConfigInterceptor } from './interceptor/httpconfig.interceptor';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChangepasswordComponent,
    ForgotpasswordComponent,
    ResetpasswordComponent,
    LeftSidebarComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    PagenotfoundComponent,
    UsersComponent,
    UserDetailsComponent,
    DiscountCodeComponent,
    StaticPagesComponent,
    StaticPagesEditComponent,
    CmsComponent,
    PinboardComponent,
    TownpinboardComponent,
    CreditPlanComponent,
    SubscriptionsComponent,
    OffersComponent,
    PlanComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    NgxMyDatePickerModule.forRoot(),
    NgxEditorModule,
    TooltipModule,
    BrowserAnimationsModule,
    EditorModule,
    CalendarModule,
    ToastModule,
    NgxSpinnerModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgxTrimDirectiveModule,
    CheckboxModule,
    GooglePlaceModule,
    ColorPickerModule,
    DropdownModule,
    NgxPaginationModule,
    MultiSelectModule


  ],
  providers: [
     { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
