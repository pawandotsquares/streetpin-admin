import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  logout(): void {
    localStorage.removeItem("SPAdminCurrentUser");
    localStorage.removeItem('token');
    localStorage.setItem('isAdminLoggedIn', "false");
    /* localStorage.clear(); */
  }
}
