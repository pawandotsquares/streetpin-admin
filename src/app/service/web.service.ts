import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { RequestOptions } from '@angular/http';
import { Discount } from 'src/app/models/discount';
import { Cms } from 'src/app/models/cms';
import { Pinboard } from 'src/app/models/pinboard';
import { Creditplan } from '../models/creditplan';
import { Subscription } from '../models/Subscription';
import { Town } from '../models/town';
import { Offers } from '../models/offers';
import * as crypto from 'crypto-js';
import { User } from 'src/app/models/user';

//const apiUrl = 'https://ylqndxd5v1.execute-api.eu-west-2.amazonaws.com/dev';
//const apiUrl = 'https://3g79de4fq7.execute-api.eu-west-2.amazonaws.com/uat';
const apiUrl = 'https://ye2xe6v0w7.execute-api.eu-west-2.amazonaws.com/production';

const secret = "@AKIAJSN26KWXJ45CJJRFUA@";
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
    })
};

@Injectable({
    providedIn: 'root'
})
export class WebService {

    constructor(private http: HttpClient) { }

    getCategories(action) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),
        };
        return this.http.post(apiUrl + `/user`, {}, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    userlogin(email, password) {
        return this.http.post(apiUrl + '/auth', { email: email, password: password }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    forgotPass(emailId) {
        return this.http.post(apiUrl + `/forgot-password`, { "email": emailId }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }

    resetPass(token, newPassword) {
        return this.http.post(apiUrl + `/forgot-password`, { "token": token, "newPassword": newPassword }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }

    changePass(email, oldPassword, newPassword) {
        return this.http.post(apiUrl + `/change-password`, { "email": email, "password": oldPassword, "newPassword": newPassword}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    getuserProfile() {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',

            }),

        };
        return this.http.post(apiUrl + `/getusers`, {}, httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    /* getDashboardData() {
        return this.http.post(apiUrl + `/getusers`, {}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    } */
    getDashboardData(action) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/pinboard`, {}, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    getuserDetails(userId) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123'

            }),

        };
        return this.http.post(apiUrl + `/getusers`, { "userId": userId }, httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    userAction(userId, action) {
        return this.http.post(apiUrl + `/action-on-user`, { "userId": userId, "action": action }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    getMasterPageList() {
        return this.http.post(apiUrl + `/get-static-master-page-list`, {}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    getPageContent(staticPageId ) {
        return this.http.post(apiUrl + `/get-static-page-list`, { "staticPageId": staticPageId, "masterPageId": 0  }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    /* getMasterPageContent(masterPageId ) {
        return this.http.post(apiUrl + `/get-static-page-list`, { staticPageId:0, "masterPageId": masterPageId  }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    } */

    editPageContent(pageData) {
        let data = {
            "staticPageId": pageData.staticPageId,
            "title": pageData.title,
            "content": pageData.content,
        }
        return this.http.post(apiUrl + `/save-static-page`, data).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }

    addDiscountVoucher(Discount: Discount, newExpDate, newStartDate): Observable<Discount> {

        Discount.startDate = newStartDate;
        Discount.expiryDate = newExpDate;

        /* body.expiryDate = newExpDate; */
        return this.http.post<Discount>(apiUrl + `/save-coupon`, Discount).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    addPinboardData(Pinboard: Pinboard, newExpDate): Observable<Pinboard> {

        Pinboard.expiryDate = newExpDate;
        /* Pinboard.logo = logo
        Pinboard.backgroundImage = backgroundImage */
        /* Pinboard.userId = 2; */
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': "add"
            }),

        };
        /* body.expiryDate = newExpDate; */
        return this.http.post<Pinboard>(apiUrl + `/pinboard`, Pinboard, httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }

    addCMSData(Cms: Cms, ImagePath=''): Observable<Cms> {
        Cms.imageUrl = ImagePath;
        /* body.expiryDate = newExpDate; */
        return this.http.post<Cms>(apiUrl + `/save-static-page`, Cms).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    getlatlng(address) {
        /* return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + "& key=AIzaSyBJlYs8kEt4twfm2B-UCAPw5gDSF-cANx4").pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        )); */

        var address1 = "address=" + address;
        var sensor = "sensor=false";
        var key = "key=AIzaSyCDt53lCO5hH0Ka6dYDLDdlRoeCIuGIq_M";
        var parameters = address1 + "&" + sensor + "&" + key;
        var output = "json";
        return this.http.get('https://maps.googleapis.com/maps/api/geocode/' + output + '?' + parameters).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));

    }
    subPlanData(Subscription: Subscription, action): Observable<Creditplan> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/subscriptions-plan`, Subscription, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    creditPlanData(Creditplan: Creditplan, action): Observable<Creditplan> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/subscriptions-plan`, Creditplan, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    creditPlanAction(planId, action) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/subscriptions-plan`, { planId: planId }, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    getOfferCategory(action) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/offer`, {}, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    addOfferData(Offers: Offers, action, newExpDate, newStartDate): Observable<Offers> {

        Offers.startDate = newStartDate;
        Offers.expiryDate = newExpDate;

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/offer`, Offers, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    offerData(Offers: Offers, action): Observable<Offers> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/offer`, Offers, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    offersAction(id, action) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/offer`, { id: id }, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }

    subscriptionsAction() {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': 'getUserSubscriptions'
            }),

        };
        return this.http.post(apiUrl + `/subscriptions-plan`, {}, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    getDiscountVoucher(couponId)  {
        return this.http.post(apiUrl + `/get-coupon-list`, {couponId: couponId}).pipe(map((response: any) =>
        JSON.parse(JSON.stringify(response))
        ));
    }
    voucherAction(couponId, action) {
        return this.http.post(apiUrl + `/action-on-coupon`, { "couponId": couponId, "action": action }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    pinboardContent(Pinboard: Pinboard, action): Observable<Pinboard> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/pinboard`, Pinboard, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    pinboardAction(id, action) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/pinboard`, { id: id }, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    getListdAction(id, action) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/pinboard`, { countryId: id }, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    allRegionelect(action) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/pinboard`, { }, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    townData(Town: Town, action): Observable<Town> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/town-pinboard`, Town, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    townAction(id, action) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': '123',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/town-pinboard`, { id: id }, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }

    cmsAction(staticPageId, action) {
        return this.http.post(apiUrl + `/action-on-static-page`, { "id": staticPageId, "action": action }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }
    refreshAuthToken(authToken, action) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'action': action
            }),

        };
        return this.http.post(apiUrl + `/user`, { authToken: authToken }, httpOptions,
        ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
        ));
    }

    setCurrentUser(currentUser: User): void {
        localStorage.setItem('SPAdminCurrentUser', this.encrypt(currentUser));
    }
    getCurrentUser(): User {
        let data = localStorage.getItem('SPAdminCurrentUser');
        if (data) {
            return new User(this.decrypt(data));
        }
        return null;

    }
   encrypt(data) {
        return crypto.AES.encrypt(JSON.stringify(data), secret,
            {
                keySize: 128 / 8,
                iv: secret,
                mode: crypto.mode.CBC,
                padding: crypto.pad.Pkcs7
            }).toString();
    }

    decrypt(data) {
        return JSON.parse(crypto.enc.Utf8.stringify(crypto.AES.decrypt(data, secret,
            {
                keySize: 128 / 8,
                iv: secret,
                mode: crypto.mode.CBC,
                padding: crypto.pad.Pkcs7
            })));
    }
}
