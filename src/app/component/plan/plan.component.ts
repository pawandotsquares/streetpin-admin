import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
import { Subscription } from 'src/app/models/Subscription';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit {
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  @Output('changeModule')
  public moduleChanged = new EventEmitter<string>();
  p: number = 1;
  SubscriptionData: object;
  htmlOptions: any = {};
  message: any;

  staticPageData: any;
  public Subscription: Subscription = new Subscription({});

  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.setHtmlOptions();
    this.getSubscriptionContent()
  }
  setHtmlOptions() {
    this.htmlOptions = {
      editMode: false
    }
  }
  addSubscription() {
    this.htmlOptions.button = false;
    this.htmlOptions.editMode = true;
    this.Subscription = new Subscription({});
  }
  editSubscription(planId) {
    this.spinner.show();
    this.htmlOptions.button = true;
    this.htmlOptions.editMode = true;
    this.Subscription.planId = planId;
    this.Subscription.planType = 1;
    this.WebService.subPlanData(this.Subscription, 'get').subscribe(resultSubscriptionData => {
      if (resultSubscriptionData['statusCode'] == 200) {
        this.spinner.hide();
        this.Subscription = new Subscription(resultSubscriptionData['responsePacket'][0])
        console.log('Subscription');
        console.log(this.Subscription);

      } else {
        this.spinner.hide();
        this.message = resultSubscriptionData;
        localStorage.setItem('statusCode', resultSubscriptionData['message']);
      }
    },
      error => {

      })

  }
  onSubmit() {
    this.spinner.show();
    this.WebService.subPlanData(this.Subscription, 'edit-pinboard-subscription-plan').subscribe(result => {
      if (result['statusCode'] == 200) {
        this.spinner.hide();
        this.message = result;
        var temp = this.htmlOptions.button;
        setTimeout(function () {
          if (temp) {
            alert("Record updated successfully");
          } else {
            alert("Record saved successfully");
          }
          window.location.reload();

        }, 500);

      } else {
        this.spinner.hide();
        this.message = result;
        alert(result['message']);
      }
    },
      error => {

      })
  }
  getSubscriptionContent() {
    this.Subscription.planId = 0;
    this.Subscription.planType = 1;
    this.WebService.subPlanData(this.Subscription, 'get').subscribe(resultSubscriptionData => {
      console.log(resultSubscriptionData);
      if (resultSubscriptionData['statusCode'] == 200) {
        this.spinner.hide();
        this.SubscriptionData = resultSubscriptionData['responsePacket']
      } else {
        this.spinner.hide();
        this.message = resultSubscriptionData;
        localStorage.setItem('statusCode', resultSubscriptionData['message']);
      }
    },
      error => {

      })
  }



  changeModule(module: string) {
    this.ngOnInit();
  }

}