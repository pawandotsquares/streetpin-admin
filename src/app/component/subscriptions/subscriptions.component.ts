import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
import { Creditplan } from 'src/app/models/creditplan';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.css']
})
export class SubscriptionsComponent implements OnInit {
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  @Output('changeModule')
  public moduleChanged = new EventEmitter<string>();
  p: number = 1;
  subscriptionData: object;
  htmlOptions: any = {};
  message: any;

  staticPageData: any;
  public Creditplan: Creditplan = new Creditplan({});

  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.subscriptionsAction()
  }

  subscriptionsAction() {
    this.spinner.show();
    this.WebService.subscriptionsAction().subscribe(resultSubscriptionActionData => {
      this.spinner.hide();
      console.log(resultSubscriptionActionData);
      if (resultSubscriptionActionData.statusCode == 200) {
        this.subscriptionData = resultSubscriptionActionData.responsePacket
      } else {
        this.message = resultSubscriptionActionData;
        localStorage.setItem('statusCode', resultSubscriptionActionData.message);
      }
    },
      error => {

      })
  }


  changeModule(module: string) {
    this.ngOnInit();
  }

}
