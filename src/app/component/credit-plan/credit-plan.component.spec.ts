import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditPlanComponent } from './credit-plan.component';

describe('CreditPlanComponent', () => {
  let component: CreditPlanComponent;
  let fixture: ComponentFixture<CreditPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
