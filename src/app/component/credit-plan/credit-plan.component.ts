import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
import { Creditplan } from 'src/app/models/creditplan';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-credit-plan',
  templateUrl: './credit-plan.component.html',
  styleUrls: ['./credit-plan.component.css']
})
export class CreditPlanComponent implements OnInit {
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  @Output('changeModule')
  public moduleChanged = new EventEmitter<string>();
  p: number = 1;
  creditplanData: object;
  htmlOptions: any = {};
  message: any;

  staticPageData: any;
  public Creditplan: Creditplan = new Creditplan({});

  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.setHtmlOptions();
    this.getCreditplanContent()
  }
  setHtmlOptions() {
    this.htmlOptions = {
      editMode: false
    }
  }
  addCreditplan() {
    this.htmlOptions.button = false;
    this.htmlOptions.editMode = true;
    this.Creditplan = new Creditplan({});
  }
  editCreditplan(planId) {
    this.spinner.show();
    this.htmlOptions.button = true;
    this.htmlOptions.editMode = true;
    this.WebService.creditPlanAction(planId, 'get').subscribe(resultCreditplanData => {
      console.log(resultCreditplanData);
      if (resultCreditplanData.statusCode == 200) {
        this.spinner.hide();
        this.Creditplan = new Creditplan(resultCreditplanData.responsePacket[0])

      } else {
        this.spinner.hide();
        this.message = resultCreditplanData;
        localStorage.setItem('statusCode', resultCreditplanData.message);
      }
    },
      error => {

      })

  }
  onSubmit() {
    this.spinner.show();
    this.WebService.creditPlanData(this.Creditplan, 'add').subscribe(result => {
      if (result['statusCode'] == 200) {
        this.spinner.hide();
        this.message = result;
        var temp = this.htmlOptions.button;
        setTimeout(function () {
          if (temp) {
            alert("Record updated successfully");
          } else {
            alert("Record saved successfully");
          }
          window.location.reload();

        }, 500);

      } else {
        this.spinner.hide();
        this.message = result;
        alert(result['message']);
      }
    },
      error => {

      })
  }
  getCreditplanContent() {
    this.WebService.creditPlanData(this.Creditplan, 'get').subscribe(resultCreditplanData => {
      console.log(resultCreditplanData);
      if (resultCreditplanData['statusCode'] == 200) {
        this.spinner.hide();
        this.creditplanData = resultCreditplanData['responsePacket']
      } else {
        this.spinner.hide();
        this.message = resultCreditplanData;
        localStorage.setItem('statusCode', resultCreditplanData['message']);
      }
    },
      error => {

      })
  }

  creditPlanAction(planId, action) {
    if (action == "delete") {
    var r = confirm("Are you sure? You will not be able to recover this in future!");
    if (r == true) {
      this.spinner.show();
      this.WebService.creditPlanAction(planId, action).subscribe(resultVoucherActionData => {
        this.spinner.hide();
        if (resultVoucherActionData.statusCode == 200) {
          setTimeout(function () {
            if (action == "active") {
              alert("Record has been activated successfully");
            } else if (action == "inactive") {
              alert("Record has been deactivated successfully");

            } else if (action == "delete") {
              alert("Record has been deleted successfully");
            }
          }, 1000);
          this.creditplanData = resultVoucherActionData.responsePacket
          this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
            this.router.navigate(["my-offer-credits"]));
        } else {
          this.message = resultVoucherActionData;
          localStorage.setItem('statusCode', resultVoucherActionData.message);
        }
      },
        error => {

        })
    }
  }else{
      this.spinner.show();
      this.WebService.creditPlanAction(planId, action).subscribe(resultVoucherActionData => {
        this.spinner.hide();
        if (resultVoucherActionData.statusCode == 200) {
          setTimeout(function () {
            if (action == "active") {
              alert("Record has been activated successfully");
            } else if (action == "inactive") {
              alert("Record has been deactivated successfully");

            } else if (action == "delete") {
              alert("Record has been deleted successfully");
            }
          }, 1000);
          this.creditplanData = resultVoucherActionData.responsePacket
          this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
            this.router.navigate(["my-offer-credits"]));
        } else {
          this.message = resultVoucherActionData;
          localStorage.setItem('statusCode', resultVoucherActionData.message);
        }
      },
        error => {

        })
  }

  }


  changeModule(module: string) {
    this.ngOnInit();
  }

}
