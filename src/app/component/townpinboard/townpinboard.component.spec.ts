import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TownpinboardComponent } from './townpinboard.component';

describe('TownpinboardComponent', () => {
  let component: TownpinboardComponent;
  let fixture: ComponentFixture<TownpinboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TownpinboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TownpinboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
