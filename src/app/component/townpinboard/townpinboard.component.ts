import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
import { Town } from 'src/app/models/town';
import { NgxSpinnerService } from 'ngx-spinner';
import { Pinboard } from 'src/app/models/pinboard';
import * as AWS from 'aws-sdk'
import { environment } from '../../../environments/environment'

@Component({
  selector: 'app-townpinboard',
  templateUrl: './townpinboard.component.html',
  styleUrls: ['./townpinboard.component.css']
})
export class TownpinboardComponent implements OnInit {

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  @Output('changeModule')
  public moduleChanged = new EventEmitter<string>();
  p: number = 1;
  townData: object;
  pinboardData: object;
  htmlOptions: any = {};
  message: any;
  countryDropdownList = [];
  userData: object;
  regionList = [];
  selectedPinboardList = [];
  suburbList = [];
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  dropdownSettingsFrUser = {};
  logo: string;
  users: any = [];
  fromTimeArray = [];
  toTimeArray = [];
  backgroundImage: string;
  imageUplaodOption: boolean;
  globalFilesArray: any = []
  staticPageData: any;
  public town: Town = new Town({});
  public pinboard: Pinboard = new Pinboard({});
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }
  openingTime: any = {
    mon: {
      checked: true,
      start: '9 AM',
      end: '10 PM'
    },
    tue: {
      checked: true,
      start: '9 AM',
      end: '10 PM'
    },
    wed: {
      checked: true,
      start: '9 AM',
      end: '10 PM'
    },
    thu: {
      checked: true,
      start: '9 AM',
      end: '10 PM'
    },
    fri: {
      checked: true,
      start: '9 AM',
      end: '10 PM'
    },
    sat: {
      checked: false,
      start: '',
      end: ''
    },
    sun: {
      checked: false,
      start: '',
      end: ''
    }
  }
  ngOnInit() {
    this.spinner.show();
    this.setHtmlOptions();
    this.getTownContent()
    this.getCountries()
    this.getPinboardContent()
    this.userDetails()
    this.populate()

    this.htmlOptions.regionList = false;
    this.htmlOptions.suburbList = false;
    this.htmlOptions.onPinboardselect = false;

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'title',
      selectAllText: 'Select All',
      unSelectAllText: 'Deselect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.dropdownSettingsFrUser = {
      singleSelection: true,
      idField: 'userId',
      textField: 'userName',
      selectAllText: 'Select All',
      unSelectAllText: 'Deselect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
  }
  setHtmlOptions() {
    this.htmlOptions = {
      editMode: false
    }
  }
  /* populate() {
    var hours, ampm;
    var hours1, ampm1;
    this.fromTimeArray.push({ hours: + "12 " + " AM" })
    for (var i = 1; i <= 11; i++) {

      this.fromTimeArray.push({ hours: i + " " + "AM" })
    }

    for (var j = 1; j <= 11; j++) {
      this.toTimeArray.push({ hours: j + " " + 'PM' })
    }
    this.toTimeArray.push({ hours: + "12 " + " PM" })
  } */
  populate() {
    var hours, ampm;
    var hours1, ampm1;
    /* this.fromTimeArray.push({ hours: + "12" }) */
    for (var i = 0; i <= 23; i++) {

      this.fromTimeArray.push({ hours: i + ":00" })
      this.fromTimeArray.push({ hours: i + ":30" })
    }

    for (var j = 0; j <= 23; j++) {
      this.toTimeArray.push({ hours: j + ":00" })
      this.toTimeArray.push({ hours: j + ":30" })
    }
    /* this.toTimeArray.push({ hours: + "12" }) */
  }
  userDetails() {
    this.WebService.getuserProfile().subscribe(resultuserData => {
      if (resultuserData.statusCode == 200) {
        this.userData = resultuserData.responsePacket
        this.users = resultuserData.responsePacket
        console.log(this.users);
      } else {
        this.message = resultuserData;
      }
    },
      error => {

      })
  }
  addTown() {
    this.htmlOptions.logo = "";
    this.htmlOptions.backgroundImage = "";
    this.htmlOptions.button = false;
    this.htmlOptions.editMode = true;
    this.town = new Town({});
  }
  editTown(planId) {
    this.spinner.show();
    this.htmlOptions.newupload = true;
    this.htmlOptions.button = true;
    this.htmlOptions.editMode = true;
    this.WebService.townAction(planId, 'getByid').subscribe(resultTownData => {

      if (resultTownData.statusCode == 200) {
        this.spinner.hide();

        this.town = new Town(resultTownData.responsePacket)
        this.openingTime = JSON.parse(resultTownData.responsePacket.businessInformation.openingHours);
        this.town.userNew = JSON.parse(resultTownData.responsePacket.user);
        this.town.user = JSON.parse(resultTownData.responsePacket.user);
        this.htmlOptions.logo = resultTownData.responsePacket.logo
        this.htmlOptions.backgroundImage = resultTownData.responsePacket.backgroundImage
        let selectedArray:any = []

        for (let i = 0; i < this.town.pinboardList.length; i++) {

          selectedArray.push({ id: this.town.pinboardList[i].id, title: this.town.pinboardList[i].title, expiryDate: this.town.pinboardList[i].expiryDate } );
        }
        this.town.pinboardList = selectedArray;
        if (this.town.regionId){
          this.htmlOptions.regionList = true;
          this.WebService.getListdAction(this.town.countryId, 'getStateList').subscribe(resultpinboardData => {
            if (resultpinboardData['statusCode'] == 200) {
              this.regionList = resultpinboardData['responsePacket']
            }
          },
            error => {

            })
        }else{
          this.htmlOptions.regionList = false;
        }
        if (this.town.suburbId) {
          this.htmlOptions.suburbList = true;
          this.WebService.getListdAction(this.town.suburbId, 'getCityList').subscribe(resultpinboardData => {
            if (resultpinboardData['statusCode'] == 200) {
              this.suburbList = resultpinboardData['responsePacket']
            }
          },
            error => {

            })
        }else{
          this.htmlOptions.suburbList = false;
        }
      } else {
        this.spinner.hide();
        this.message = resultTownData;
        localStorage.setItem('statusCode', resultTownData.message);
      }
    },
      error => {

      })

  }
  getPinboardContent() {
    this.WebService.pinboardContent(this.pinboard, 'getAll').subscribe(resultpinboardData => {

      if (resultpinboardData['statusCode'] == 200) {
        this.spinner.hide();
        this.dropdownList = []
       var dropdownListObj = resultpinboardData['responsePacket']

        for (let i = 0; i < dropdownListObj.length; i++) {
          this.dropdownList.push({ label: dropdownListObj[i].title, value: { id: dropdownListObj[i].id, title: dropdownListObj[i].title, expiryDate: dropdownListObj[i].expiryDate } });
        }
        console.log(this.dropdownList)
      } else {
        this.spinner.hide();
        this.message = resultpinboardData;
        localStorage.setItem('statusCode', resultpinboardData['message']);
      }
    },
      error => {

      })
  }
  getCountries() {
    this.WebService.pinboardContent(this.pinboard, 'getCountryList').subscribe(resultpinboardData => {

      if (resultpinboardData['statusCode'] == 200) {
        this.spinner.hide();
       this.countryDropdownList = resultpinboardData['responsePacket']
      } else {
        this.spinner.hide();
        this.message = resultpinboardData;
        localStorage.setItem('statusCode', resultpinboardData['message']);
      }
    },
      error => {

      })
  }
  handleAddressChange(event: any) {
    this.town.location = '';
    this.town.country = '';
    this.town.region = '';
    this.town.suburb = '';
    this.town.location = event.formatted_address;
    this.getCountry(event.address_components);
    this.WebService.getlatlng(event.formatted_address).subscribe(resultTownData => {
      if (resultTownData.status == "OK") {
        this.town.latitude = resultTownData.results[0].geometry.location.lat;
        this.town.longitude = resultTownData.results[0].geometry.location.lng;
        this.spinner.hide();
        this.townData = resultTownData['responsePacket']
      } else {
        this.spinner.hide();
        this.town.latitude = '';
        this.town.longitude = '';
        this.message = resultTownData;
        localStorage.setItem('statusCode', resultTownData['message']);
      }
    },
      error => {

      })
  }
  getCountry(addrComponents) {
  for (var i = 0; i < addrComponents.length; i++) {
    if (addrComponents[i].types[0] == "country") {
      this.town.country = addrComponents[i].long_name;
    }
    if (addrComponents[i].types[0] == "administrative_area_level_1") {
      this.town.region = addrComponents[i].long_name;
    }
    if (addrComponents[i].types[0] == "administrative_area_level_2") {
      this.town.suburb = addrComponents[i].long_name;
    }
    /* if (addrComponents[i].types.length == 2) {
      if (addrComponents[i].types[0] == "political") {
        this.town.region =  addrComponents[i].short_name;
      }
    } */
  }
    console.log(this.town);
}
  fileEvent(fileInput: any, fileType) {
    this.htmlOptions.newupload = false;
    var files = fileInput.target.files;
    var file = files[0];
    if (file.size > 5242880) {
      this.town.logo = "";
      this.town.backgroundImage = "";
      alert("Image size should be less than 5MB");
      this.globalFilesArray = [];
      return false;
    } else if (file.type == "image/jpeg" || file.type == "image/JPEG" || file.type == "image/png" || file.type == "image/PNG" || file.type == "image/jpg" || file.type == "image/JPG") {

      var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)
      if (pos != -1) {
        if (file == undefined) {
          this.globalFilesArray.splice(pos, 1);
        } else {
          this.globalFilesArray[pos].fileType = fileType
          this.globalFilesArray[pos].file = file
        }
      } else {

        this.globalFilesArray.push({ fileType: fileType, file: file })
      }
      if (fileType == 'logo') {
        this.htmlOptions.logo = ""
      }
      if (fileType == 'backgroundImage') {
        this.htmlOptions.backgroundImage = ""
      }
    } else {
      this.town.logo = "";
      this.town.backgroundImage = "";
      alert("Only jpg, png and jpeg file format allowed");
      this.globalFilesArray = [];
      return false;
    }

  }
  multipleProductImageUploadl(callbackError, callbackSuccess, ) {

    var _that = this
    AWS.config.update({
      region: environment.aws_region,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: environment.aws_IdentityPoolId
      })
    })

    const s3 = new AWS.S3({
      apiVersion: environment.aws_apiVersion,
      params: { Bucket: environment.aws_bucketName }
    })

    var ImagesUrls = []
    if (this.globalFilesArray.length > 0) {
      this.globalFilesArray.map((item) => {
        var params = {
          Bucket: environment.aws_bucketName,
          Key: '_cmsImages/' + new Date().getTime() + "__" + item.fileType + "__" + item.file.name,
          Body: item.file,
          ACL: 'public-read'
        };
        s3.upload(params, function (err, data) {
          if (err) {
            callbackError(err)
          } else {
            let fType = data.key.split("__")[1]
            ImagesUrls.push({ ImagePath: data.Location, Id: 0, ImageType: fType })
            if (ImagesUrls.length == _that.globalFilesArray.length) {
              callbackSuccess(ImagesUrls)
            }
          }
        })
      })
    } else {
      callbackSuccess("")
    }
  }
  onSubmit() {
    if (this.town.user) {
      this.town.userId = this.town.user['0']['userId']
    }
    this.spinner.show();
    let self = this;
    this.multipleProductImageUploadl(
      function (error) {
        if (this.htmlOptions.editMode) {
          this.submitWithoutImage()
        } else {
          alert(error)
        }
      },
      function (success) {
        if (success == '') {
          self.logo = self.town.logo
          self.backgroundImage = self.town.backgroundImage
        } else {

          for (let i = 0; i < success.length; i++) {

            if (success[i].ImageType == "logo") {
              self.town.logo = success[i].ImagePath
            }

            if (success[i].ImageType == "backgroundImage") {
              self.town.backgroundImage = success[i].ImagePath
            }
          }

        }
        /* self.town.pinboardList = []; */
        /* console.log('pinboardList');
        console.log(self.town.pinboardList); */
        var myJSON = JSON.stringify(self.openingTime);
        self.town.businessInformation.openingHours = myJSON;
        self.town.user = self.town.userNew
        var user = JSON.stringify(self.town.user);
        self.town.user = user;

        self.WebService.townData(self.town, 'add').subscribe(result => {
      if (result['statusCode'] == 200) {
        self.spinner.hide();
        self.message = result;
        var temp = self.htmlOptions.button;
        setTimeout(function () {
          if (temp) {
            alert("Record updated successfully");
          } else {
            alert("Record saved successfully");
          }
          window.location.reload();

        }, 500);

      } else {
        self.message = result;
      }
    },
      error => {

      })

      })
  }
  getTownContent() {
    this.WebService.townData(this.town, 'getAll').subscribe(resultTownData => {

      if (resultTownData['statusCode'] == 200) {
        this.spinner.hide();
        this.townData = resultTownData['responsePacket']
      } else {
        this.spinner.hide();
        this.message = resultTownData;
        localStorage.setItem('statusCode', resultTownData['message']);
      }
    },
      error => {

      })
  }

 townAction(planId, action) {
   if (action == "delete") {
     var r = confirm("Are you sure? You will not be able to recover this in future!");
     if (r == true) {
       this.spinner.show();
       this.WebService.townAction(planId, action).subscribe(resultVoucherActionData => {
         this.spinner.hide();
         if (resultVoucherActionData.statusCode == 200) {
           setTimeout(function () {
             if (action == "active") {
               alert("Record has been activated successfully");
             } else if (action == "inactive") {
               alert("Record has been deactivated successfully");

             } else if (action == "delete") {
               alert("Record has been deleted successfully");
             }
           }, 1000);
           this.townData = resultVoucherActionData.responsePacket
           this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
             this.router.navigate(["town-pinboard"]));
         } else {
           this.message = resultVoucherActionData;
           localStorage.setItem('statusCode', resultVoucherActionData.message);
         }
       },
         error => {

         })
     }
   } else {
     this.spinner.show();
     this.WebService.townAction(planId, action).subscribe(resultVoucherActionData => {
       this.spinner.hide();
       if (resultVoucherActionData.statusCode == 200) {
         setTimeout(function () {
           if (action == "active") {
             alert("Record has been activated successfully");
           } else if (action == "inactive") {
             alert("Record has been deactivated successfully");

           } else if (action == "delete") {
             alert("Record has been deleted successfully");
           }
         }, 1000);
         this.townData = resultVoucherActionData.responsePacket
         this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
           this.router.navigate(["town-pinboard"]));
       } else {
         this.message = resultVoucherActionData;
         localStorage.setItem('statusCode', resultVoucherActionData.message);
       }
     },
       error => {

       })
   }

  }

  onCountrySelect(item: any) {
    this.WebService.getListdAction(item, 'getStateList').subscribe(resultpinboardData => {
      if (resultpinboardData['statusCode'] == 200) {
        this.spinner.hide();
        this.htmlOptions.regionList = true;

        this.regionList = resultpinboardData['responsePacket']
      } else {
        this.spinner.hide();
        this.message = resultpinboardData;
        localStorage.setItem('statusCode', resultpinboardData['message']);
      }
    },
      error => {

      })
  }
  onRegionelect(item: any) {
    this.WebService.getListdAction(item, 'getCityList').subscribe(resultpinboardData => {
      if (resultpinboardData['statusCode'] == 200) {
        this.spinner.hide();
        this.htmlOptions.suburbList = true;
        this.suburbList = resultpinboardData['responsePacket']
      } else {
        this.spinner.hide();
        this.message = resultpinboardData;
        localStorage.setItem('statusCode', resultpinboardData['message']);
      }
    },
      error => {

      })
  }

  onItemSelect(item: any) {

  }
  onPinboardselect(item: any) {
    if(item.length == 0){
      this.htmlOptions.onPinboardselect = true;
       return false;
    }else{
      this.htmlOptions.onPinboardselect = false;
    }
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  changeModule(module: string) {
    this.ngOnInit();
  }

}



