import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebService } from '../../service/web.service'
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  resetPassForm: FormGroup;
  submitted = false;
  message: any;
  confirmPassNotValid: boolean;
  token: any;
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  constructor(
    private formBuilder: FormBuilder,
    private dataservice: WebService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.token = params['id'];
    });
    this.resetPassForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_password: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {

    return this.resetPassForm.controls;

  }
  onSubmit() {

    this.submitted = true;

    if (this.f.password.value != this.f.confirm_password.value) {
      this.confirmPassNotValid = true;
      return;
    } else {
      this.confirmPassNotValid = false;
    }

    // stop here if form is invalid
    if (this.resetPassForm.invalid) {
      return;
    }
    this.spinner.show();
    if (this.f.password.value) {

      let password = this.f.password.value;

      //call login api here
      this.dataservice.resetPass(this.token, password).subscribe(result => {
        this.spinner.hide();
        if (result.statusCode == 200) {
          this.message = result;
          setTimeout(function () {
          alert("Thanks for changing your password, Please login");
        }, 500);
        this.router.navigateByUrl('');
        } else {
          this.message = result;
          localStorage.setItem('statusCode', result.message);
        }

      },
        error => {

        })
    } else {
      alert('Error Occurred.');
    }
  }
}
