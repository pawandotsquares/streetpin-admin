import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebService } from '../../service/web.service'
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  changePassForm: FormGroup;
  submitted = false;
  message: any;
  confirmPassNotValid: boolean;
  token: any;
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  constructor(
    private formBuilder: FormBuilder,
    private dataservice: WebService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.changePassForm = this.formBuilder.group({
      old_password: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_password: ['', Validators.required]
    }, { validator: this.checkIfMatchingPasswords('password', 'confirm_password') });
  }
  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {

    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordConfirmationInput.value) {
        if (passwordInput.value !== passwordConfirmationInput.value) {
          return passwordConfirmationInput.setErrors({ notEquivalent: true })
        } else {
          return passwordConfirmationInput.setErrors(null);
        }
      }
    }
  }
  // convenience getter for easy access to form fields
  get f() {

    return this.changePassForm.controls;

  }
  onSubmit() {

    this.submitted = true;

    if (this.f.password.value != this.f.confirm_password.value) {
      this.confirmPassNotValid = true;
      return;
    } else {
      this.confirmPassNotValid = false;
    }
    // stop here if form is invalid
    if (this.changePassForm.invalid) {
      return;
    }
    this.spinner.show();

    if (this.f.password.value) {

      let oldPassword =this.f.old_password.value;

      let newPassword = this.f.password.value;
      let email = localStorage.getItem('email');

      //call login api here
      this.dataservice.changePass(email, oldPassword, newPassword).subscribe(result => {
        this.spinner.hide();
        if (result.statusCode == 200) {
          setTimeout(function () {
          this.message = result;
          alert("Password changed successfully");
        }, 500);
        this.router.navigateByUrl('/dashboard');
        } else {
          this.message = result;
        }

      },
        error => {

        })
    } else {
      alert('Error Occurred.');
    }
  }
}
