import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;
  submitted = false;
  message: any;
  email: any;
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  constructor(
    private formBuilder: FormBuilder,
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService

  ) { }

  ngOnInit() {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }
  // convenience getter for easy access to form fields
  get f() {

    return this.forgotPasswordForm.controls;

  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.forgotPasswordForm.invalid) {
      return;
    }
    this.spinner.show();
    if (this.f.email.value) {

      let email = this.f.email.value;

      //call login api here
      this.WebService.forgotPass(email).subscribe(result => {
        this.spinner.hide();
        if (result.statusCode == 200) {
          this.message = result;
          this.forgotPasswordForm.reset();
          localStorage.setItem('statusCode', result.message);
          setTimeout(function () {
          alert("Email has been sent");
        }, 500);
        this.router.navigateByUrl('');
        } else {
          this.message = result;
          localStorage.setItem('statusCode', result.message);
        }
      },
        error => {

        })
    } else {
      alert('Invalid Email id.');
    }
  }
}
