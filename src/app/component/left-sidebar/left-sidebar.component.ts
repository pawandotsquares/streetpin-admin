import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.css']
})
export class LeftSidebarComponent implements OnInit {

  @Output('changeModule')
  moduleChanged = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  changeModule(module: string) {
    this.moduleChanged.emit(module);
  }

}
