import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
import { Offers } from 'src/app/models/offers';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { Pinboard } from 'src/app/models/pinboard';
import * as AWS from 'aws-sdk'
import { environment } from '../../../environments/environment'

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit {

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  @Output('changeModule')
  public moduleChanged = new EventEmitter<string>();
  p: number = 1;
  offerData: object;
  htmlOptions: any = {};
  message: any;
  userData: object;
  dropdownSettings = {};
  logo: string;
  backgroundImage: string;
  dropdownList = [];
  pinboardArray = [];
  categorydata: any;
  offerLocationData: object;
  public datepipe: DatePipe;
  staticPageData: any;
  offerIcon: string;
  offerMedia: string;
  minDate: Date;
  maxDate: Date;
  globalFilesArray: any = []
  /* public Offers: Offers = new Offers({}); */
  public Offers: Offers = {} as Offers;
  public pinboard: Pinboard = new Pinboard({});
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.minDate = new Date();
    this.maxDate = new Date();
    this.setHtmlOptions();
    this.getofferContent()
    this.userDetails()
    this.getCategories();
    this.getPinboardContent()
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'title',
      selectAllText: 'Select All',
      unSelectAllText: 'Deselect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }
  setHtmlOptions() {
    this.htmlOptions = {
      editMode: false
    }
  }
  addOffer() {
    this.htmlOptions.button = false;
    this.htmlOptions.editMode = true;
    this.htmlOptions.offerMedia = "";
    this.htmlOptions.offerIcon = "";
    this.Offers = {} as Offers;
    this.Offers.categoryId = null
    this.Offers.userId = null
    /* this.Offers = this.Offers; */
  }
  editoffer(id) {
    this.spinner.show();
    this.htmlOptions.newupload = true;
    this.htmlOptions.button = true;
    this.htmlOptions.editMode = true;
    this.WebService.offersAction(id, 'getByid').subscribe(resultofferData => {

      if (resultofferData.statusCode == 200) {
        this.spinner.hide();
        this.Offers =resultofferData.responsePacket
        this.htmlOptions.offerIcon = resultofferData.responsePacket.offerIcon
        this.htmlOptions.offerMedia = resultofferData.responsePacket.offerMedia
        var newVarSDate = this.Offers.startDate.toString();
        var newSDateArr = newVarSDate.split("-");
        var newStartDate = newSDateArr[2] + "-" + newSDateArr[1] + "-" + newSDateArr[0];
        this.Offers.startDate = new Date(parseInt(newSDateArr[2]), parseInt(newSDateArr[1]) - 1, parseInt(newSDateArr[0]));
        //this.Offers.startDate = new Date(newStartDate);

        var newVarDate = this.Offers.expiryDate.toString();
        var newDateArr = newVarDate.split("-");
        var newExpDate = newDateArr[2] + "-" + newDateArr[1] + "-" + newDateArr[0];
        this.Offers.expiryDate = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[0]));
        //this.Offers.expiryDate = new Date(newExpDate);

      } else {
        this.spinner.hide();
        this.message = resultofferData;
        localStorage.setItem('statusCode', resultofferData.message);
      }
    },
      error => {

      })

  }
  getPinboardContent() {
    this.WebService.pinboardContent(this.pinboard, 'getAll').subscribe(resultpinboardData => {
      if (resultpinboardData['statusCode'] == 200) {
        this.dropdownList = resultpinboardData['responsePacket']
      } else {
        this.message = resultpinboardData;
        localStorage.setItem('statusCode', resultpinboardData['message']);
      }
    },
      error => {

      })
  }
  colorCode(event: any){
    this.Offers.backgroundColor = event;
  }
  userDetails() {
    this.WebService.getuserProfile().subscribe(resultuserData => {
      if (resultuserData.statusCode == 200) {
        this.userData = resultuserData.responsePacket
      } else {
        this.message = resultuserData;
      }
    },
      error => {

      })
  }
  handleAddressChange(event: any) {
    this.Offers.location = event.formatted_address;
    this.WebService.getlatlng(event.formatted_address).subscribe(resultOffersData => {
      if (resultOffersData.status == "OK") {
        this.Offers.latitude = resultOffersData.results[0].geometry.location.lat;
        this.Offers.longitude = resultOffersData.results[0].geometry.location.lng;
        this.spinner.hide();
        this.offerLocationData = resultOffersData['responsePacket']
      } else {
        this.spinner.hide();
        this.Offers.latitude = '';
        this.Offers.longitude = '';
        this.message = resultOffersData;
        localStorage.setItem('statusCode', resultOffersData['message']);
      }
    },
      error => {

      })
  }
  fileEvent(fileInput: any, fileType) {
    this.htmlOptions.newupload = false;
    var files = fileInput.target.files;
    var file = files[0];
    if (file.size > 5242880) {
      this.Offers.offerIcon = "";
      this.Offers.offerMedia = "";
      alert("Image size should be less than 5MB");
      this.globalFilesArray = [];
      return false;
    } else if (file.type == "image/jpeg" || file.type == "image/JPEG" || file.type == "image/png" || file.type == "image/PNG" || file.type == "image/jpg" || file.type == "image/JPG") {
      var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

      if (pos != -1) {
        if (file == undefined) {
          this.globalFilesArray.splice(pos, 1);
        } else {
          this.globalFilesArray[pos].fileType = fileType
          this.globalFilesArray[pos].file = file
        }
      } else {
        this.globalFilesArray.push({ fileType: fileType, file: file })
      }

      if (fileType == 'offerIcon') {
        this.htmlOptions.offerIcon = ""
      }
      if (fileType == 'offerMedia') {
        this.htmlOptions.offerMedia = ""
      }
    } else {

      this.Offers.offerIcon = "";
      this.Offers.offerMedia = "";
      alert("Only jpg, png and jpeg file format allowed");
      this.globalFilesArray = [];
      return false;
    }

  }
  multipleProductImageUploadl(callbackError, callbackSuccess, ) {

    var _that = this
    AWS.config.update({
      region: environment.aws_region,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: environment.aws_IdentityPoolId
      })
    })

    const s3 = new AWS.S3({
      apiVersion: environment.aws_apiVersion,
      params: { Bucket: environment.aws_bucketName }
    })

    var ImagesUrls = []
    if (this.globalFilesArray.length > 0) {
      this.globalFilesArray.map((item) => {
        var params = {
          Bucket: environment.aws_bucketName,
          Key: '_cmsImages/' + new Date().getTime() + "__" + item.fileType + "__" + item.file.name,
          Body: item.file,
          ACL: 'public-read'
        };
        s3.upload(params, function (err, data) {
          if (err) {
            callbackError(err)
          } else {
            let fType = data.key.split("__")[1]
            ImagesUrls.push({ ImagePath: data.Location, Id: 0, ImageType: fType })
            if (ImagesUrls.length == _that.globalFilesArray.length) {
              callbackSuccess(ImagesUrls)
            }
          }
        })
      })
    } else {
      callbackSuccess("")
    }
  }
  backgroundColor(event:any){
    this.Offers.backgroundColor = event;
  }
  //category listing
	getCategories() {
		this.WebService.getOfferCategory('getOfferCategory').subscribe(resultcategoryData => {
			if (resultcategoryData['statusCode'] == 200) {
				this.categorydata = resultcategoryData['responsePacket'];
			}
		},
		error => {

		})
	}
  onSubmit() {
    if (this.Offers.pinboardObjList){
      for (let i = 0; i < this.Offers.pinboardObjList.length; i++) {
        this.pinboardArray.push(this.Offers.pinboardObjList[i]['id']);
      }
    }
    this.Offers.pinboardList = this.pinboardArray;
    this.spinner.show();
    let self = this;
    this.multipleProductImageUploadl(
      function (error) {
        if (this.htmlOptions.editMode) {
          this.submitWithoutImage()
        } else {
          alert(error)
        }
      },
      function (success) {
        if (success == '') {
          /* self.offerIcon = self.Offers.offerIcon
          self.offerMedia = self.Offers.offerMedia */
        } else {

          for (let i = 0; i < success.length; i++) {

            if (success[i].ImageType == "offerIcon") {
              self.Offers.offerIcon = success[i].ImagePath
            }

            if (success[i].ImageType == "offerMedia") {
              self.Offers.offerMedia = success[i].ImagePath
            }
          }

        }
    var s = self.Offers.startDate;
    var curr_date = s.getDate();
    var curr_month = s.getMonth() + 1; //Months are zero based
    var curr_year = s.getFullYear();
    var newStartDate = curr_date + "-" + curr_month + "-" + curr_year;

    var e = self.Offers.expiryDate;
    var curr_date = e.getDate();
    var curr_month = e.getMonth() + 1; //Months are zero based
    var curr_year = e.getFullYear();
    var newExpDate = curr_date + "-" + curr_month + "-" + curr_year;
    //self.Offers.adminUserId = parseInt(localStorage.getItem('userId'));
    self.WebService.addOfferData(self.Offers, 'add', newExpDate, newStartDate).subscribe(result => {
      self.spinner.hide();
      if (result['statusCode'] == 200) {
        self.message = result;
        var temp = self.htmlOptions.button;
        setTimeout(function () {
          if (temp) {
            alert("Record updated successfully");
          } else {
            alert("Record saved successfully");
          }
          window.location.reload();

        }, 500);

      } else {
        alert(result['message']);
        self.message = result;
      }
    },
      error => {

      })
      })
  }
  getofferContent() {
    this.WebService.offerData(this.Offers, 'getAll').subscribe(resultofferData => {
      this.spinner.hide();
      if (resultofferData['statusCode'] == 200) {
        this.offerData = resultofferData['responsePacket']
      } else {
        this.message = resultofferData;
        localStorage.setItem('statusCode', resultofferData['message']);
      }
    },
      error => {

      })
  }

  offerAction(id, action) {
    if (action == "delete") {
      var r = confirm("Are you sure? You will not be able to recover this in future!");
      if (r == true) {
        this.spinner.show();
        this.WebService.offersAction(id, action).subscribe(resultVoucherActionData => {
          this.spinner.hide();
          if (resultVoucherActionData.statusCode == 200) {
            setTimeout(function () {
              if (action == "active") {
                alert("Record has been activated successfully");
              } else if (action == "inactive") {
                alert("Record has been deactivated successfully");

              } else if (action == "delete") {
                alert("Record has been deleted successfully");
              }
            }, 1000);
            this.offerData = resultVoucherActionData.responsePacket
            this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
              this.router.navigate(["offers"]));
          } else {
            this.message = resultVoucherActionData;
            localStorage.setItem('statusCode', resultVoucherActionData.message);
          }
        },
          error => {

          })
      }
    } else {
      this.spinner.show();
      this.WebService.offersAction(id, action).subscribe(resultVoucherActionData => {
        this.spinner.hide();
        if (resultVoucherActionData.statusCode == 200) {
          setTimeout(function () {
            if (action == "active") {
              alert("Record has been activated successfully");
            } else if (action == "inactive") {
              alert("Record has been deactivated successfully");

            } else if (action == "delete") {
              alert("Record has been deleted successfully");
            }
          }, 1000);
          this.offerData = resultVoucherActionData.responsePacket
          this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
            this.router.navigate(["offers"]));
        } else {
          this.message = resultVoucherActionData;
          localStorage.setItem('statusCode', resultVoucherActionData.message);
        }
      },
        error => {

        })
    }

  }

  changeModule(module: string) {
    this.ngOnInit();
  }
  setMinTodate(){
    this.maxDate = this.Offers.startDate
    this.Offers.expiryDate = this.Offers.startDate
  }
  onItemSelect(item: any) {

  }
  onPinboardselect(item: any) {
    if (item.length == 0) {
      this.htmlOptions.onPinboardselect = true;
      return false;
    } else {
      this.htmlOptions.onPinboardselect = false;
    }
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onCategoryChange(e: any) {
    let selectElementText = e.target['options']
    [e.target['options'].selectedIndex].text;
    this.Offers.category = selectElementText;
  }

}
