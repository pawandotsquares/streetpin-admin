import { Component, OnInit } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router, Route, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  userData: object;
  message: any;
  userId: number;
  noData: boolean;
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  constructor(
    private WebService: WebService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {

   }

  ngOnInit() {
    this.spinner.show();
     this.route.params.subscribe(params => {
      this.userId = +params['id'];
      this.userDetails(this.userId)
    });
  }

  userDetails(userId) {
    this.WebService.getuserDetails(userId).subscribe(resultuserData => {
      this.spinner.hide();
      if (resultuserData.statusCode == 200) {
        this.userData = resultuserData.responsePacket
        console.log(this.userData);
      } else {
        alert("No Record Found");
        this.message = resultuserData;
        this.router.navigateByUrl('/users');
      }
    },
      error => {

      })
  }
}
