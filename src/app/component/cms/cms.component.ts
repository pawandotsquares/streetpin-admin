import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
import { Cms } from 'src/app/models/cms';
import { NgxSpinnerService } from 'ngx-spinner';
import * as AWS from 'aws-sdk'
import { environment } from '../../../environments/environment'
(window as any).global = window;
@Component({
  selector: 'app-cms',
  templateUrl: './cms.component.html',
  styleUrls: ['./cms.component.css']
})
export class CmsComponent implements OnInit {
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  @Output('changeModule')
  public moduleChanged = new EventEmitter<string>();
  p: number = 1;
  cmsData: object;
  htmlOptions: any = {};
  message: any;
  cmsViewData: any;
  imageUrl: string;
  masterPageName:string;
  masterPageId:string;
  staticPageData: any;
  imageUplaodOption : boolean;
  public Cms: Cms = new Cms({});
  editorConfig = {
    "editable": true,
    "spellcheck": true,
    "height": "200px",
    "minHeight": "0",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "Enter text here...",
    "imageEndPoint": "/src/assets/images/content/",
    "toolbar": [
      ["bold", "italic"],
      ["fontName", "fontSize"],
      /* ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"], */
      ["cut", "copy", "delete"],
      /*  ["paragraph","orderedList", "unorderedList"], */
      /* ["image"] */
      /* ["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
      ["fontName", "fontSize", "color"],
      ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
      ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
      ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
      ["link", "unlink", "image", "video"] */
    ],
  }
  globalFilesArray:any=[]
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.imageUplaodOption = false;
    this.spinner.show();
    this.setHtmlOptions();
    this.getCMSContent()
    this.getMasterPageList();
  }
  setHtmlOptions() {
    this.htmlOptions = {
      editMode: false
    }
  }
  addCms() {
    this.htmlOptions.button = false;
    this.htmlOptions.editMode = true;
    this.Cms = new Cms({});
    this.Cms.masterPageId = null;
  }
  editCMS(staticPageId, masterPageId) {
    this.htmlOptions.newupload = true;
    if (masterPageId == 1) {
      this.imageUplaodOption = true;
    } else if (masterPageId == 2) {
      this.imageUplaodOption = false;
    } else if (masterPageId == 3) {
      this.imageUplaodOption = true;
    } else {
      this.imageUplaodOption = false;
    }
    this.spinner.show();
    this.htmlOptions.button = true;
    this.htmlOptions.editMode = true;
    this.WebService.getPageContent(staticPageId).subscribe(resultcmsData => {
      /* console.log(resultcmsData); */
      if (resultcmsData.statusCode == 200) {
        this.spinner.hide();
        this.Cms = new Cms(resultcmsData.responsePacket[0])

      } else {
        this.spinner.hide();
        this.message = resultcmsData;
        localStorage.setItem('statusCode', resultcmsData.message);
      }
    },
      error => {

      })

  }
  viewCMS(staticPageId) {
    this.cmsViewData = "";
    this.spinner.show();
    this.htmlOptions.button = true;
    this.htmlOptions.viewMode = true;

    this.WebService.getPageContent(staticPageId).subscribe(resultViewCmsData => {
      if (resultViewCmsData.statusCode == 200) {
        this.spinner.hide();
        this.cmsViewData = resultViewCmsData.responsePacket
        this.masterPageName = resultViewCmsData.responsePacket[0].masterPageName;
        this.masterPageId = resultViewCmsData.responsePacket[0].masterPageId;
        this.imageUrl = resultViewCmsData.responsePacket[0].imageUrl;

      } else {
        this.spinner.hide();
        this.message = resultViewCmsData;
        localStorage.setItem('statusCode', resultViewCmsData.message);
      }
    },
      error => {

      })

  }
  onSubmit() {
    this.spinner.show();
    if (this.imageUplaodOption){
      this.submitWithImage();
    }else{
      this.submitWithoutImage()
    }

  }
  submitWithImage(){
    let self = this;
    this.multipleProductImageUploadl(
      function (error) {
        if (this.htmlOptions.editMode){
          this.submitWithoutImage()
        }else{
          alert(error)
        }
      },
      function (success) {
        if (success==''){
          var imagePath = self.Cms.imageUrl
        }else{
          imagePath = success[0].ImagePath
        }

        self.WebService.addCMSData(self.Cms, imagePath).subscribe(result => {
      if (result['statusCode'] == 200) {
        self.spinner.hide();
        self.message = result;
        var temp = self.htmlOptions.button;
        setTimeout(function () {
          if (temp) {
            alert("Record updated successfully");
          } else {
            alert("Record saved successfully");
          }
          window.location.reload();

        }, 500);

      } else {
        self.message = result;
      }
    },
      error => {

      })
      }
    )
  }
  submitWithoutImage(){
    this.WebService.addCMSData(this.Cms).subscribe(result => {
      if (result['statusCode'] == 200) {
        this.spinner.hide();
        this.message = result;
        var temp = this.htmlOptions.button;
        setTimeout(function () {
          if (temp) {
            alert("Record updated successfully");
          } else {
            alert("Record saved successfully");
          }
          window.location.reload();

        }, 500);

      } else {
        this.message = result;
      }
    },
      error => {

      })
  }

  getCMSContent(staticPageId=0) {
    this.spinner.show();
    this.WebService.getPageContent(staticPageId).subscribe(resultcmsData => {
      if (resultcmsData.statusCode == 200) {
        this.cmsData = resultcmsData.responsePacket
        this.spinner.hide();
      } else {
        this.message = resultcmsData;
        this.spinner.hide();
        localStorage.setItem('statusCode', resultcmsData.message);
      }
    },
      error => {

      })
  }
  getMasterPageList() {
    this.WebService.getMasterPageList().subscribe(resultStaticPageData => {
      if (resultStaticPageData.statusCode == 200) {
        this.staticPageData = resultStaticPageData.responsePacket
      } else {
        this.message = resultStaticPageData;
        localStorage.setItem('statusCode', resultStaticPageData.message);
      }
    },
      error => {

      })
  }
  cmsAction(voucherId, action) {
    if (action == "delete"){
    var r = confirm("Are you sure? You will not be able to recover this in future!");
    if (r == true) {
      this.spinner.show();
      this.WebService.cmsAction(voucherId, action).subscribe(resultVoucherActionData => {
        this.spinner.hide();
        if (resultVoucherActionData.statusCode == 200) {
          setTimeout(function () {
            if (action == "active") {
              alert("Record has been activated successfully");
            } else if (action == "inactive") {
              alert("Record has been deactivated successfully");

            } else if (action == "delete") {
              alert("Record has been deleted successfully");
            }
          }, 1000);
          this.cmsData = resultVoucherActionData.responsePacket
          this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
            this.router.navigate(["cms"]));
        } else {
          this.message = resultVoucherActionData;
          localStorage.setItem('statusCode', resultVoucherActionData.message);
        }
      },
        error => {

        })
      }
    }else{
      this.spinner.show();
      this.WebService.cmsAction(voucherId, action).subscribe(resultVoucherActionData => {
        this.spinner.hide();
        if (resultVoucherActionData.statusCode == 200) {
          setTimeout(function () {
            if (action == "active") {
              alert("Record has been activated successfully");
            } else if (action == "inactive") {
              alert("Record has been deactivated successfully");

            } else if (action == "delete") {
              alert("Record has been deleted successfully");
            }
          }, 1000);
          this.cmsData = resultVoucherActionData.responsePacket
          this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
            this.router.navigate(["cms"]));
        } else {
          this.message = resultVoucherActionData;
          localStorage.setItem('statusCode', resultVoucherActionData.message);
        }
      },
        error => {

        })
    }

    }


    changeModule(module: string) {
      this.ngOnInit();
  }

  fileEvent(fileInput: any, fileType) {
    this.htmlOptions.newupload = false;
    var files = fileInput.target.files;
    var file = files[0];

    if (file.size > 5242880) {
      this.Cms.imageUrl ="";
      alert("Image size should be less than 5MB");
      this.globalFilesArray = [];
      return false;
    }else if (file.type == "image/jpeg" || file.type == "image/JPEG" || file.type == "image/png" || file.type == "image/PNG" || file.type == "image/jpg" || file.type == "image/JPG") {
      var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

      if (pos != -1) {
        if (file == undefined) {
          this.globalFilesArray.splice(pos, 1);
        } else {
          this.globalFilesArray[pos].fileType = fileType
          this.globalFilesArray[pos].file = file
        }
      } else {
        this.globalFilesArray.push({ fileType: fileType, file: file })
      }
    }else{

      this.Cms.imageUrl ="";
      alert("Only jpg, png and jpeg file format allowed");
      this.globalFilesArray = [];
      return false;
    }

  }
  getType(pageId: any) {
    if (pageId == 1) {
      this.imageUplaodOption = true;
    } else if (pageId == 2) {
      this.imageUplaodOption = false;
    } else if (pageId == 3) {
      this.imageUplaodOption = true;
    } else {
      this.imageUplaodOption = false;
    }

  }
  multipleProductImageUploadl(callbackError, callbackSuccess, ) {

    var _that = this
    AWS.config.update({
      region: environment.aws_region,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: environment.aws_IdentityPoolId
      })
    })

    const s3 = new AWS.S3({
      apiVersion: environment.aws_apiVersion,
      params: { Bucket: environment.aws_bucketName }
    })

    var ImagesUrls = []
    if (this.globalFilesArray.length > 0) {
      this.globalFilesArray.map((item) => {
        var params = {
          Bucket: environment.aws_bucketName,
          Key: '_cmsImages/' + new Date().getTime() + "__" + item.fileType + "__" + item.file.name,
          Body: item.file,
          ACL: 'public-read'
        };
        s3.upload(params, function (err, data) {
          if (err) {
            callbackError(err)
          } else {
            let fType = data.key.split("__")[1]
            ImagesUrls.push({ ImagePath: data.Location, Id: 0, ImageType: fType })
            if (ImagesUrls.length == _that.globalFilesArray.length) {
              callbackSuccess(ImagesUrls)
            }
          }
        })
      })
    } else {
      callbackSuccess("")
    }
  }
  onTextChange(event){
    console.log(event);
  }

}
