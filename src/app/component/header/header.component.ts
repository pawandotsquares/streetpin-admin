import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { Router } from '@angular/router';
import { WebService } from '../../service/web.service'
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public User = new User({});
  constructor(
    private authService: AuthService,
    public router: Router,
    public WebService: WebService
  ) { }

  ngOnInit() {
    if (localStorage.getItem("LoggedInTime")) {
      var t0 = Number(localStorage.getItem("LoggedInTime"));
      if (isNaN(t0)) t0 = 0;
      var t1 = new Date().getTime();
      var duration = t1 - t0;
      if (duration < 55 * 60 * 1000) {
        if (duration > 40 * 60 * 1000) {
          this.refreshAuthToken();
          var loginTime = new Date().getTime();
          localStorage.setItem("LoggedInTime", loginTime.toString());
       }
      } else {
        alert("Your session has been expired, Please log in again.");
        this.logout();
      }
    }
  }
  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
  refreshAuthToken() {
    var token = this.WebService.getCurrentUser().authToken;
    this.WebService.refreshAuthToken(token, 'refreshAuthToken').subscribe(resultProfileData => {
      if (resultProfileData['statusCode'] == 200) {
        console.log();
        var authToken = resultProfileData['responsePacket'].authToken;
        this.getUserData(authToken)
      }
    },
      error => {

      })
  }
  getUserData(authToken){
    this.WebService.getuserProfile().subscribe(resultProfileData => {
      if (resultProfileData['statusCode'] == 200) {
        this.User = new User(resultProfileData['responsePacket'])
        this.User.authToken = authToken;
        this.WebService.setCurrentUser(this.User);
      }
    },
      error => {

      })
  }
}
