import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WebService } from '../../service/web.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from '../../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  message: any;
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  public User = new User({});
  constructor(
    private formBuilder: FormBuilder,
    private webService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    if (localStorage.getItem('isLoggedIn') == "true") {
       this.router.navigateByUrl('/dashboard');
    }
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      rememberme: ['']
    });

    var loginFormObj = {};
    let remEmail = localStorage.getItem('remEmail');
    let remPass = localStorage.getItem('remPass');
    let getRememberMe = localStorage.getItem('rememberMe');
    if (getRememberMe) {

      loginFormObj = {
        email: remEmail,
        password: remPass,
        rememberme: true
      };

      this.loginForm.setValue(loginFormObj);
    }
  }

  // convenience getter for easy access to form fields
  get f() {

    return this.loginForm.controls;

  }
  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.spinner.show();
    let rememberMe = this.f.rememberme.value;

    if (this.f.email.value) {

      let email = this.f.email.value;
      let remPassword = this.f.password.value;

      let password = this.f.password.value;

      //call login api here
      this.webService.userlogin(email, password).subscribe(loginResult => {
        this.spinner.hide();
        if (loginResult.statusCode == 200) {
          this.User = new User(loginResult.responsePacket)
          this.webService.setCurrentUser(this.User);
          console.log(this.User);
          this.setLoggedInTime();
          this.message = loginResult;
          localStorage.setItem('email', email);
          if (rememberMe) {
            localStorage.setItem('rememberMe', rememberMe);
            localStorage.setItem('remEmail', email);
            localStorage.setItem('email', email);
            localStorage.setItem('remPass', remPassword);
          } else {
            localStorage.setItem('rememberMe', '');
            localStorage.setItem('remEmail', '');
            localStorage.setItem('remPass', '');
          }

          localStorage.setItem('user_role', "admin");
          localStorage.setItem('isAdminLoggedIn', "true");
          localStorage.setItem('userId', loginResult.responsePacket.userId);

          let pageUrl = localStorage.getItem('pageUrl');
          if (pageUrl) {
            this.router.navigateByUrl(pageUrl);
            localStorage.setItem('pageUrl', "");
          } else {
            this.router.navigateByUrl('/dashboard');
          }
        } else {
          this.message = loginResult;
          localStorage.setItem('statusCode', loginResult.message);
        }

      },
        error => {

        })
    } else {
      alert('Credentials are incorrect.');
    }

  }
  setLoggedInTime() {
    var loginTime = new Date().getTime();
    localStorage.setItem("LoggedInTime", loginTime.toString())
  }

}
