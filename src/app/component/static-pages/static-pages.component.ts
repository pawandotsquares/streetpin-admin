import { Component, OnInit } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-static-pages',
  templateUrl: './static-pages.component.html',
  styleUrls: ['./static-pages.component.css']
})
export class StaticPagesComponent implements OnInit {
  pageData: object;
  message: any;
  noData: boolean;
  constructor(
    private WebService: WebService,
    private router: Router
  ) { }

  ngOnInit() {
    let userId = localStorage.getItem('userId');
    this.pagesDetails()
  }
  pagesDetails() {
    this.WebService.getMasterPageList().subscribe(resultPageData => {
      console.log(resultPageData);
      if (resultPageData.statusCode == 200) {
        this.pageData = resultPageData.responsePacket
      } else {
        this.message = resultPageData;
        localStorage.setItem('statusCode', resultPageData.message);
      }
    },
      error => {

      })
  }
}
