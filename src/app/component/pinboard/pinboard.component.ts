import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { WebService } from '../../service/web.service'
import { NgxSpinnerService } from 'ngx-spinner';
import { Pinboard } from 'src/app/models/pinboard';
import { Router } from '@angular/router';
import * as AWS from 'aws-sdk'
import { BusinessInformation } from 'src/app/models/businessInformation';
import { environment } from '../../../environments/environment'

@Component({
  selector: 'app-pinboard',
  templateUrl: './pinboard.component.html',
  styleUrls: ['./pinboard.component.css']
})
export class PinboardComponent implements OnInit {

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  @Output('changeModule')
  public moduleChanged = new EventEmitter<string>();
  minDate: Date;
  p: number = 1;
  name: any;
  pinboardViewData: any;
  logo: string;
  backgroundImage: string;
  dropdownSettings = {};
  typeText: any;
  newVarDate: any;
  userData: object;
  pinboardData: object;
  htmlOptions: any = {};
  imageUplaodOption: boolean;
  categorydata: any;
  /* openingHours: string[] = ['mon', 'tue', 'wed', 'thu', 'fri']; */
  fromTimeArray = [];
  toTimeArray = [];
  users: any= [];
  /* public businessInformation: BusinessInformation = {} as BusinessInformation; */
  message: any;

  openingTime: any = {
    mon: {
      checked: true,
      start: '9 AM',
      end: '10 PM'
    },
    tue: {
      checked: true,
      start: '9 AM',
      end: '10 PM'
    },
    wed: {
      checked: true,
      start: '9 AM',
      end: '10 PM'
    },
    thu: {
      checked: true,
      start: '9 AM',
      end: '10 PM'
    },
    fri: {
      checked: true,
      start: '9 AM',
      end: '10 PM'
    },
    sat: {
      checked: false,
      start: '',
      end: ''
    },
    sun: {
      checked: false,
      start: '',
      end: ''
    }
  }

  staticPageData: any;
  public pinboard: Pinboard = new Pinboard({});
  globalFilesArray: any = []
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.fromTimeArray = [];
    this.toTimeArray = [];
    this.minDate = new Date();
    this.spinner.show();
    this.setHtmlOptions();
    this.getPinboardContent()
    this.userDetails()
    this.populate()
    this.getCategories();

    this.dropdownSettings = {
      singleSelection: true,
      idField: 'userId',
      textField: 'userName',
      selectAllText: 'Select All',
      unSelectAllText: 'Deselect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
  }
  fileEvent(fileInput: any, fileType) {
    this.htmlOptions.newupload = false;
    var files = fileInput.target.files;
    var file = files[0];
    if (file.size > 5242880) {
      this.pinboard.backgroundImage = "";
      this.pinboard.logo = "";
      alert("Image size should be less than 5MB");
      this.globalFilesArray = [];
      return false;
    } else if (file.type == "image/jpeg" || file.type == "image/JPEG" || file.type == "image/png" || file.type == "image/PNG" || file.type == "image/jpg" || file.type == "image/JPG") {
      var pos = this.globalFilesArray.map(function (e) { return e.fileType; }).indexOf(fileType)

      if (pos != -1) {
        if (file == undefined) {
          this.globalFilesArray.splice(pos, 1);
        } else {
          this.globalFilesArray[pos].fileType = fileType
          this.globalFilesArray[pos].file = file
        }
      } else {
        this.globalFilesArray.push({ fileType: fileType, file: file })
      }
      if (fileType == 'logo'){
        this.htmlOptions.logo = ""
      }
      if (fileType == 'backgroundImage'){
        this.htmlOptions.backgroundImage = ""
      }

    } else {

      this.pinboard.backgroundImage = "";
      this.pinboard.logo = "";
      alert("Only jpg, png and jpeg file format allowed");
      this.globalFilesArray = [];
      return false;
    }

  }
  /* populate() {
    var hours, ampm;
    var hours1, ampm1;
    this.fromTimeArray.push({ hours: + "12 " + " AM" })
    for (var i = 1; i <= 11; i ++) {

      this.fromTimeArray.push({ hours: i + " " + "AM" })
    }

    for (var j = 1; j <= 11; j ++) {
      this.toTimeArray.push({ hours: j + " " + 'PM' })
    }
    this.toTimeArray.push({ hours: + "12 " + " PM" })
  } */
  /* populate() {
		var hours, ampm;
		var hours1, ampm1;
		this.fromTimeArray.push({ hours: + "12 " + " AM" })
		for (var i = 1; i <= 11; i++) {

			this.fromTimeArray.push({ hours: i + ":00 " + "AM" })
			this.fromTimeArray.push({ hours: i + ":30 " + "AM" })
		}

		for (var j = 1; j <= 11; j++) {
			this.toTimeArray.push({ hours: j + ":00 " + 'PM' })
			this.toTimeArray.push({ hours: j + ":30 " + 'PM' })
		}
		this.toTimeArray.push({ hours: + "12 " + " PM" })
	}
 */
populate() {
		var hours, ampm;
		var hours1, ampm1;
		/* this.fromTimeArray.push({ hours: + "12" }) */
		for (var i = 0; i <= 23; i++) {

			this.fromTimeArray.push({ hours: i + ":00" })
			this.fromTimeArray.push({ hours: i + ":30" })
		}

	 for (var j = 0; j <= 23; j++) {
			this.toTimeArray.push({ hours: j + ":00" })
			this.toTimeArray.push({ hours: j + ":30" })
		}
		/* this.toTimeArray.push({ hours: + "12" }) */
	}
  fromChange(event: any) {
    console.log(event);

  }
  multipleProductImageUploadl(callbackError, callbackSuccess, ) {

    var _that = this
    AWS.config.update({
      region: environment.aws_region,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: environment.aws_IdentityPoolId
      })
    })

    const s3 = new AWS.S3({
      apiVersion: environment.aws_apiVersion,
      params: { Bucket: environment.aws_bucketName }
    })

    var ImagesUrls = []
    if (this.globalFilesArray.length > 0) {
      this.globalFilesArray.map((item) => {
        var params = {
          Bucket: environment.aws_bucketName,
          Key: '_cmsImages/' + new Date().getTime() + "__" + item.fileType + "__" + item.file.name,
          Body: item.file,
          ACL: 'public-read'
        };
        s3.upload(params, function (err, data) {
          if (err) {
            callbackError(err)
          } else {
            let fType = data.key.split("__")[1]
            ImagesUrls.push({ ImagePath: data.Location, Id: 0, ImageType: fType })
            if (ImagesUrls.length == _that.globalFilesArray.length) {
              callbackSuccess(ImagesUrls)
            }
          }
        })
      })
    } else {
      callbackSuccess("")
    }
  }
  setHtmlOptions() {
    this.htmlOptions = {
      editMode: false
    }
  }
  getCategories() {
    this.WebService.getCategories('getCategory').subscribe(resultcategoryData => {
      if (resultcategoryData['statusCode'] == 200) {
        this.categorydata = resultcategoryData['responsePacket'];
      } else {
        /* this.toastr.error(resultSubscriptionData['message']); */
      }
    },
      error => {

      })
  }
  addPinboard() {
    this.htmlOptions.logo = "";
    this.htmlOptions.backgroundImage = "";
    this.htmlOptions.button = false;
    this.htmlOptions.editMode = true;
    this.pinboard = new Pinboard({});
    this.pinboard.categoryId = null;
    this.pinboard.type = null;
    /* this.pinboard.businessInformation.openingHours = this.openingHours; */
  }
  editPinboard(id) {
    this.htmlOptions.newupload = true;
    this.spinner.show();
    this.htmlOptions.button = true;
    this.htmlOptions.editMode = true;
    this.WebService.pinboardAction(id, 'getById').subscribe(resultpinboardData => {
      this.spinner.hide();

      if (resultpinboardData.statusCode == 200) {
        this.pinboard = new Pinboard(resultpinboardData.responsePacket)
        this.pinboard.type = parseInt(resultpinboardData.responsePacket.type)
        //this.openingTime = JSON.parse(resultpinboardData.responsePacket.businessInformation.openingHours);

        if (JSON.parse(resultpinboardData.responsePacket.businessInformation.openingHours)){
          this.openingTime = JSON.parse(resultpinboardData.responsePacket.businessInformation.openingHours)
        }
        console.log(this.openingTime);
        this.pinboard.userNew = JSON.parse(resultpinboardData.responsePacket.user);
        this.pinboard.user = JSON.parse(resultpinboardData.responsePacket.user);
        this.htmlOptions.logo = resultpinboardData.responsePacket.logo
        this.htmlOptions.backgroundImage = resultpinboardData.responsePacket.backgroundImage
        var newVarDate = this.pinboard.expiryDate.toString();
        var newDateArr = newVarDate.split("-");
        var newExpDate = newDateArr[2] + "-" + newDateArr[1] + "-" + newDateArr[0];
        this.pinboard.expiryDate = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1]) - 1, parseInt(newDateArr[0]));

      } else {
        this.message = resultpinboardData;
        localStorage.setItem('statusCode', resultpinboardData.message);
      }
    },
      error => {

      })

  }
  onFilterChange(event: any, day: any){
    if (!event){

    }
  }
  userDetails() {
    this.WebService.getuserProfile().subscribe(resultuserData => {
      if (resultuserData.statusCode == 200) {
        this.users = resultuserData.responsePacket
      } else {
        this.message = resultuserData;
      }
    },
      error => {

      })
  }
  onSubmit() {

    this.spinner.show();

    let self = this;
    this.multipleProductImageUploadl(
      function (error) {
        if (this.htmlOptions.editMode) {
          this.submitWithoutImage()
        } else {
          alert(error)
        }
      },
      function (success) {
        if (success == '') {
          self.logo = self.pinboard.logo
          self.backgroundImage = self.pinboard.backgroundImage
        } else {

          for (let i = 0; i < success.length; i++) {

            if (success[i].ImageType == "logo") {
              self.pinboard.logo = success[i].ImagePath
            }

            if (success[i].ImageType == "backgroundImage") {
              self.pinboard.backgroundImage = success[i].ImagePath
            }
          }

        }

        if (typeof self.pinboard.expiryDate === 'object'){
          var e = self.pinboard.expiryDate;
          var curr_date = e.getDate();
          var curr_month = e.getMonth() + 1; //Months are zero based
          var curr_year = e.getFullYear();
          var newExpDate = curr_date + "-" + curr_month + "-" + curr_year;
        }else{
           newExpDate = self.pinboard.expiryDate;
        }


        var myJSON = JSON.stringify(self.openingTime);
        self.pinboard.businessInformation.openingHours = myJSON;
        self.pinboard.user = self.pinboard.userNew
        self.pinboard.userId = 0;
        if (self.pinboard.userNew && self.pinboard.userNew.length>0){
          self.pinboard.userId = self.pinboard.userNew[0].userId
        }
        /* if (self.pinboard.userNew){

        } */
        var user = JSON.stringify(self.pinboard.user);
        self.pinboard.isAllowPosts = true;
        self.pinboard.user = user;
        self.pinboard.pinboardStatus = 2;
        self.pinboard.adminUserId = localStorage.getItem('userId');
        if(self.pinboard.type==2){
          self.pinboard.isPaid = true;
        }else{
          self.pinboard.isPaid = false;
        }
        self.WebService.addPinboardData(self.pinboard, newExpDate).subscribe(result => {

          self.spinner.hide();
          if (result['statusCode'] == 200) {
            self.message = result;
            var temp = self.htmlOptions.button;
            setTimeout(function () {
              if (temp) {
                alert("Record updated successfully");
              } else {
                alert("Record saved successfully");
              }
              window.location.reload();

            }, 500);

          } else {
            self.message = result;
            alert(result['message'])
          }
        },
          error => {

          })

      }
    )
  }
  getPinboardContent() {
    this.WebService.pinboardContent(this.pinboard, 'getAll').subscribe(resultpinboardData => {
      console.log(resultpinboardData);
      if (resultpinboardData['statusCode'] == 200) {
        this.spinner.hide();
        this.pinboardData = resultpinboardData['responsePacket']
      } else {
        this.spinner.hide();
        this.message = resultpinboardData;
        localStorage.setItem('statusCode', resultpinboardData['message']);
      }
    },
      error => {

      })
  }
  handleAddressChange(event: any) {

    this.pinboard.businessInformation.location = event.formatted_address;
    this.WebService.getlatlng(event.formatted_address).subscribe(resultpinboardData => {
      if (resultpinboardData.status == "OK") {
        this.pinboard.latitude = resultpinboardData.results[0].geometry.location.lat;
        this.pinboard.longitude = resultpinboardData.results[0].geometry.location.lng;
        this.spinner.hide();
        this.pinboardData = resultpinboardData['responsePacket']
      } else {
        this.spinner.hide();
        this.pinboard.latitude = '';
        this.pinboard.longitude = '';
        this.message = resultpinboardData;
        localStorage.setItem('statusCode', resultpinboardData['message']);
      }
    },
      error => {

      })
  }
  onItemSelect(item: any) {
    //this.pinboard.userId = item.userId;
  }
  onPinboardselect(item: any) {

  }
  pinboardAction(id, action) {
    if (action == "delete") {
      var r = confirm("Are you sure? You will not be able to recover this in future!");
      if (r == true) {
        this.spinner.show();
        this.WebService.pinboardAction(id, action).subscribe(resultPinBoardActionData => {
          this.spinner.hide();
          console.log(resultPinBoardActionData);
          if (resultPinBoardActionData.statusCode == 200) {
            setTimeout(function () {
              if (action == "active") {
                alert("Record has been activated successfully");
              } else if (action == "inactive") {
                alert("Record has been deactivated successfully");

              } else if (action == "delete") {
                alert("Record has been deleted successfully");
              }
            }, 1000);
            this.pinboardData = resultPinBoardActionData.responsePacket
            this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
              this.router.navigate(["pinboard"]));
          } else {
            this.message = resultPinBoardActionData;
            alert(resultPinBoardActionData.message);
            localStorage.setItem('statusCode', resultPinBoardActionData.message);
          }
        },
          error => {

          })
      }
    } else {
      this.spinner.show();
      this.WebService.pinboardAction(id, action).subscribe(resultPinBoardActionData => {
        this.spinner.hide();
        if (resultPinBoardActionData.statusCode == 200) {
          setTimeout(function () {
            if (action == "active") {
              alert("Record has been activated successfully");
            } else if (action == "inactive") {
              alert("Record has been deactivated successfully");

            } else if (action == "delete") {
              alert("Record has been deleted successfully");
            }
          }, 1000);
          this.pinboardData = resultPinBoardActionData.responsePacket
          this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
            this.router.navigate(["pinboard"]));
        } else {
          this.message = resultPinBoardActionData;
          localStorage.setItem('statusCode', resultPinBoardActionData.message);
        }
      },
        error => {

        })
    }

  }
  viewPinboard(id) {
    this.pinboardViewData = "";
    this.spinner.show();
    this.htmlOptions.button = true;
    this.htmlOptions.viewMode = true;
    this.WebService.pinboardAction(id, 'getById').subscribe(resultViewPinboardData => {

      if (resultViewPinboardData.statusCode == 200) {
        this.spinner.hide();
        this.pinboard = new Pinboard(resultViewPinboardData.responsePacket)
        /* this.openingTime = JSON.parse(resultViewPinboardData.responsePacket.businessInformation.openingHours); */
        this.pinboard.user = JSON.parse(resultViewPinboardData.responsePacket.user);
        this.name = '';
        if (this.pinboard.user.length>0){
          this.name = this.pinboard.user[0].email
        }
        if (JSON.parse(resultViewPinboardData.responsePacket.businessInformation.openingHours)) {
          this.openingTime = JSON.parse(resultViewPinboardData.responsePacket.businessInformation.openingHours)
        }
        this.pinboardViewData = resultViewPinboardData.responsePacket
        if (resultViewPinboardData.responsePacket.type == 1) {
          this.typeText = "Free"
        } else {
          this.typeText = "Paid"
        }

      } else {
        this.spinner.hide();
        this.message = resultViewPinboardData;
        localStorage.setItem('statusCode', resultViewPinboardData.message);
      }
    },
      error => {

      })

  }


  changeModule(module: string) {
    this.ngOnInit();
  }
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}

