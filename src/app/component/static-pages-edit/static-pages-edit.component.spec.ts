import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticPagesEditComponent } from './static-pages-edit.component';

describe('StaticPagesEditComponent', () => {
  let component: StaticPagesEditComponent;
  let fixture: ComponentFixture<StaticPagesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticPagesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticPagesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
