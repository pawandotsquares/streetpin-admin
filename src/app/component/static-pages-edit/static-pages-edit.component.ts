import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WebService } from '../../service/web.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-static-pages-edit',
  templateUrl: './static-pages-edit.component.html',
  styleUrls: ['./static-pages-edit.component.css']
})
export class StaticPagesEditComponent implements OnInit {
  pageDataForm: FormGroup;
  text: string;
  message: object;
  profileDataEdit: object;
  submitted = false;
  profileData: any;
  firstName: any;
  base64textString: any;
  editFromimg: any;

  editorConfig= {
    "editable": true,
    "spellcheck": true,
    "height": "200px",
    "minHeight": "0",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "Enter text here...",
    "imageEndPoint": "",
    "toolbar": [
    ["bold", "italic"],
    ["fontName", "fontSize"],
    /* ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"], */
    ["cut", "copy", "delete"],
    /*  ["paragraph","orderedList", "unorderedList"], */
    ["image"]
    /* ["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
    ["fontName", "fontSize", "color"],
    ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
    ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
    ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
    ["link", "unlink", "image", "video"] */
  ]
}


  constructor(
    private formBuilder: FormBuilder,
    private DataService: WebService,
    private route: ActivatedRoute,
    private Router: Router,
  ) {
    this.route.params.subscribe(params => this.getPageDetailsByID(params['id']));
   }
  getPageDetailsByID(id: any) {

    this.DataService.getMasterPageList().subscribe(pageDetails => {
      console.log(pageDetails);
      var editFormArr = {};
      if (pageDetails.statusCode == 200) {

        editFormArr = {
          staticPageId: pageDetails.responsePacket[0].staticPageId,
          title: pageDetails.responsePacket[0].title,
          content: pageDetails.responsePacket[0].content,

        };
        this.editFromimg = pageDetails.responsePacket[0].profileImage;
        this.pageDataForm.setValue(editFormArr);
      }
    }

    )
  }
  ngOnInit() {
    this.pageDataForm = this.formBuilder.group({
      staticPageId: [''],
      title: ['', Validators.required],
      content: ['', Validators.required]

    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.pageDataForm.controls;
  }
  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.pageDataForm.invalid) {
      return;
    }

    //call login api here

    this.DataService.editPageContent(this.pageDataForm.value).subscribe(result => {

      if (result.statusCode == 200) {
        this.message = result;
        this.Router.navigateByUrl('/pages');
      } else {
        this.message = result;
      }
    },
      error => {

      })
  }
}
