import { Component, OnInit } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  dashboardData: object;
  message: any;
  noData: boolean;
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    let userId = localStorage.getItem('userId');
    this.dashboardDetails()
  }
  dashboardDetails() {
    this.WebService.getDashboardData("getAdminDashboardInfo").subscribe(resultDashboardData => {
      this.spinner.hide();
      if (resultDashboardData.statusCode == 200) {
        this.dashboardData = resultDashboardData.responsePacket
      } else {
        this.message = resultDashboardData;
        localStorage.setItem('statusCode', resultDashboardData.message);
      }
    },
      error => {

      })
  }
}
