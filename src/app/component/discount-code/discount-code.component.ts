import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
import { Discount } from 'src/app/models/discount';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-discount-code',
  templateUrl: './discount-code.component.html',
  styleUrls: ['./discount-code.component.css']
})
export class DiscountCodeComponent implements OnInit {
  @Output('changeModule')
  public moduleChanged = new EventEmitter<string>();
  voucherData: object;
  htmlOptions: any = {};
  p: number = 1;
  message: any;
  value: Date;
  noData: boolean;
  flat: boolean;
  minDate: Date;
  maxDate: Date;
  percent: boolean;
  credit: boolean;
  public Discount: Discount = new Discount({});
  public datepipe: DatePipe;
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  /* messageService: any; */
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService

  ) { }

  ngOnInit() {
    this.Discount.offerFor = null
    this.spinner.show();
    this.minDate = new Date();
    this.maxDate = new Date();
    this.flat = false;
    this.percent = false;
    this.credit = false;
    this.setHtmlOptions();
    let userId = localStorage.getItem('userId');
    this.getDiscountVoucher()
  }

  setHtmlOptions() {
    this.htmlOptions = {
      editMode: false
    }
  }

  addDiscount() {
    this.htmlOptions.button = false;
    this.htmlOptions.editMode = true;
    this.Discount = new Discount({});
    this.Discount.offerFor = null
    this.Discount.couponType = null
  }

  editDiscount(couponId) {
    this.htmlOptions.button = true;
    this.htmlOptions.editMode = true;
    this.spinner.show();
    this.WebService.getDiscountVoucher(couponId).subscribe(resultVoucherData => {
      if (resultVoucherData.statusCode == 200) {
        this.spinner.hide();
        this.Discount = new Discount(resultVoucherData.responsePacket[0])
        if (this.Discount.couponType == 1) {
          this.flat = true;
          this.percent = false;
          this.credit = false;
        } else if (this.Discount.couponType == 2) {
          this.flat = false;
          this.percent = true;
          this.credit = false;
        } else if (this.Discount.couponType == 3) {
          this.flat = false;
          this.percent = false;
          this.credit = true;
        } else {
          this.flat = false;
          this.percent = false;
          this.credit = false;
        }

        var newVarSDate = this.Discount.startDate.toString();
        var newSDateArr = newVarSDate.split("-");
        var newStartDate = newSDateArr[2] + "-" + newSDateArr[1] + "-" + newSDateArr[0];
        this.Discount.startDate = new Date(parseInt(newSDateArr[2]), parseInt(newSDateArr[1]) - 1, parseInt(newSDateArr[0]));
        //this.Discount.startDate = new Date(newStartDate);

        var newVarDate = this.Discount.expiryDate.toString();
        var newDateArr = newVarDate.split("-");
        var newExpDate = newDateArr[2] + "-" + newDateArr[1] + "-" + newDateArr[0];
        this.Discount.expiryDate = new Date(parseInt(newDateArr[2]), parseInt(newDateArr[1])-1, parseInt(newDateArr[0]) );
        //this.Discount.expiryDate = new Date(newExpDate);


      } else {
        this.spinner.hide();
        this.message = resultVoucherData;
        localStorage.setItem('statusCode', resultVoucherData.message);
      }
    },
      error => {

      })

  }
  onSubmit() {
    this.spinner.show();
    /* this.voucherForm.value, this.voucherForm.value.expiryDate.formatted*/
    //call login api here
    var newStartDate;
    var newExpDate;
      var s = this.Discount.startDate;
      if(typeof s == 'object'){
        var curr_date = s.getDate();
        var curr_month = s.getMonth() + 1; //Months are zero based
        var curr_year = s.getFullYear();
        newStartDate = curr_date + "-" + curr_month + "-" + curr_year;

        var e = this.Discount.expiryDate;
        var curr_date = e.getDate();
        var curr_month = e.getMonth() + 1; //Months are zero based
        var curr_year = e.getFullYear();
        newExpDate = curr_date + "-" + curr_month + "-" + curr_year;
      }else{
        newStartDate = this.Discount.startDate;
        newExpDate = this.Discount.expiryDate;
      }




    this.WebService.addDiscountVoucher(this.Discount, newExpDate, newStartDate).subscribe(result => {
      this.spinner.hide();
      if (result['statusCode'] == 200) {
        this.message = result;
        var temp = this.htmlOptions.button;
        setTimeout(function () {
        if (temp){
          alert("Record updated successfully");
        }else{
          alert("Record saved successfully");
        }
          window.location.reload();

        }, 500);

      } else {
        this.spinner.hide();
        setTimeout(function () {
          alert(result['message']);
        }, 500);
        this.message = result;
      }
    },
      error => {

      })
  }
  getDiscountVoucher(couponId = 0) {
    this.WebService.getDiscountVoucher(couponId).subscribe(resultVoucherData => {
      this.spinner.hide();
      if (resultVoucherData.statusCode == 200) {
        this.voucherData = resultVoucherData.responsePacket
      } else {
        this.message = resultVoucherData;
        localStorage.setItem('statusCode', resultVoucherData.message);
      }
    },
      error => {

      })
  }
  voucherAction(voucherId, action) {
    if (action == "delete") {
      var r = confirm("Are you sure? You will not be able to recover this in future!");
      if (r == true) {
        this.spinner.show();
        this.WebService.voucherAction(voucherId, action).subscribe(resultVoucherActionData => {
          this.spinner.hide();
          if (resultVoucherActionData.statusCode == 200) {
            setTimeout(function () {
              if (action == "active") {
                alert("Record has been activated successfully");
              } else if (action == "inactive") {
                alert("Record has been deactivated successfully");

              } else if (action == "delete") {
                alert("Record has been deleted successfully");
              }
            }, 1000);
            this.voucherData = resultVoucherActionData.responsePacket
            this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
              this.router.navigate(["discountvouchers"]));
          } else {
            this.spinner.hide();
            this.message = resultVoucherActionData;
            localStorage.setItem('statusCode', resultVoucherActionData.message);
          }
        },
          error => {

          })
      }
    } else {
      this.spinner.show();
      this.WebService.voucherAction(voucherId, action).subscribe(resultVoucherActionData => {
        this.spinner.hide();
        if (resultVoucherActionData.statusCode == 200) {
          setTimeout(function () {
            if (action == "active") {
              alert("Record has been activated successfully");
            } else if (action == "inactive") {
              alert("Record has been deactivated successfully");

            } else if (action == "delete") {
              alert("Record has been deleted successfully");
            }
          }, 1000);
          this.voucherData = resultVoucherActionData.responsePacket
          this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
            this.router.navigate(["discountvouchers"]));
        } else {
          this.spinner.hide();
          this.message = resultVoucherActionData;
          localStorage.setItem('statusCode', resultVoucherActionData.message);
        }
      },
        error => {

        })
    }

  }
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  getType(vouchertpye: any){
    if(vouchertpye == 1){
      this.flat = true;
      this.percent = false;
      this.credit = false;
    } else if (vouchertpye == 2){
      this.flat = false;
      this.percent = true;
      this.credit = false;
    } else if (vouchertpye == 3){
      this.flat = false;
      this.percent = false;
      this.credit = true;
    }else{
      this.flat = false;
      this.percent = false;
      this.credit = false;
    }

  }
  changeModule(module: string) {
    this.ngOnInit();
  }
  setMinTodate() {
    this.maxDate = this.Discount.startDate
    this.Discount.expiryDate = this.Discount.startDate

  }

}
