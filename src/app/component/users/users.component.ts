import { Component, OnInit } from '@angular/core';
import { WebService } from '../../service/web.service'
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  p: number = 1;
  userData: object;
  message: any;
  noData: boolean;
  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  constructor(
    private WebService: WebService,
    private router: Router,
    private spinner: NgxSpinnerService

  ) { }

  ngOnInit() {
    this.spinner.show();
    let userId = localStorage.getItem('userId');
    this.userDetails()
  }
  userDetails() {
    this.WebService.getuserProfile().subscribe(resultuserData => {
      this.spinner.hide();
      if (resultuserData.statusCode == 200) {
        this.userData = resultuserData.responsePacket
      } else {
        this.message = resultuserData;
        localStorage.setItem('statusCode', resultuserData.message);
      }
    },
      error => {

      })
  }
  userAction(userId, action) {
    if (action == "delete") {
      var r = confirm("Are you sure? You will not be able to recover this in future!");
      if (r == true) {
        this.spinner.show();
        this.WebService.userAction(userId, action).subscribe(resultuserData => {
          this.spinner.hide();
          if (resultuserData.statusCode == 200) {
            setTimeout(function () {
              if (action == "active") {
                alert("Record has been activated successfully");
              } else if (action == "inactive") {
                alert("Record has been deactivated successfully");

              } else if (action == "delete") {
                alert("Record has been deleted successfully");
              }
            }, 1000);
            this.userData = resultuserData.responsePacket
            this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
              this.router.navigate(["users"]));
          } else {
            this.message = resultuserData;
            localStorage.setItem('statusCode', resultuserData.message);
          }
        },
          error => {

          })
      }
    } else {
      this.spinner.show();
      this.WebService.userAction(userId, action).subscribe(resultuserData => {
        this.spinner.hide();
        if (resultuserData.statusCode == 200) {
          setTimeout(function () {
            if (action == "active") {
              alert("Record has been activated successfully");
            } else if (action == "inactive") {
              alert("Record has been deactivated successfully");

            } else if (action == "delete") {
              alert("Record has been deleted successfully");
            }
          }, 1000);
          this.userData = resultuserData.responsePacket
          this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
            this.router.navigate(["users"]));
        } else {
          this.message = resultuserData;
          localStorage.setItem('statusCode', resultuserData.message);
        }
      },
        error => {

        })
    }

  }
}
