import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { WebService } from '../service/web.service'


@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
    helper:any = new JwtHelperService();
    constructor(
        public userService: WebService,
        private router: Router,
        private authService: AuthService,
    ) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authToken: string = this.userService.getCurrentUser() ? this.userService.getCurrentUser().authToken : '';

        const isExpired = authToken ? this.helper.isTokenExpired(authToken) : false;

        if (isExpired && localStorage.getItem('isAdminLoggedIn')) {
            //this.userService.logout();
            alert("Your session has been expired, Please log in again.");
            this.authService.logout();
            this.router.navigate(['/login']);
            //this.router.navigate([''], { queryParams: { returnUrl: request.url } });

        }
        if (request.method == "POST") {
            if (authToken) {
                request = request.clone({ headers: request.headers.set('Authorization', authToken) });
            }

            if (!request.headers.has('Content-Type')) {
                request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
            }
        }
        request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                   // console.log('event--->>>', event);
                    // this.errorDialogService.openDialog(event);
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                let data = {};
                data = {
                    reason: error && error.error.reason ? error.error.reason : '',
                    status: error.status
                };
                // this.errorDialogService.openDialog(data);
                return throwError(error);
            }));
    }
}