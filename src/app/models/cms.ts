export class Cms {
    public staticPageId: number;
    public masterPageId: number;
    public masterPageName: number;
    public title: String = "";
    public content: string = "";
    public imageUrl: string = "";


    constructor(params: any) {
        Object.assign(this, params);
    }
}