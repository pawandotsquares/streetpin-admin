export class Discount {

    public couponId: number;
    public title: String = "";
    public couponType: number;
    public offerFor: any;
    public couponCode: string = "";
    public couponValue: number;
    public description: string = "";;
    public numberOfUsers: number;
    public startDate: Date;
    public expiryDate: Date;

    constructor(params: any) {
        Object.assign(this, params);
    }


}