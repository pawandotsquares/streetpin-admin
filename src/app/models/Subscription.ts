export class Subscription {
    public planId: number;
    public planAmount: number;
    public planName: String = "";
    public planType: any = "";
    public amountDescription: any = "";
    public description: any = "";

    constructor(params: any) {
        Object.assign(this, params);
    }


}