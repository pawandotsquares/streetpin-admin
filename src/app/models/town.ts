import { BusinessInformation } from "./businessInformation";
export class Town {
    public title: String = "";
    public backgroundImage: string = "";
    public logo: string = "";
    public latitude: String = "";
    public longitude: String = "";
    public countryId: number;
    public regionId: number;
    public suburbId: number;
    public location: string;
    public country: string;
    public region: string;
    public suburb: string;
    public pinboardList: any;
    public userId: number;
    public user: any;
    public userNew: any;
    public businessInformation: BusinessInformation;

    constructor(params: any) {
        Object.assign(this, params);
        let businessInformation = params && params.businessInformation ? params.businessInformation : {};
        this.businessInformation = businessInformation as BusinessInformation
    }


}