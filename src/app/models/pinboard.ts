import { BusinessInformation } from "./businessInformation";

export class Pinboard {
    public id: number;
    public userId: number;
    public adminUserId: any;
    public user: any;
    public categoryId: any;
    public userNew: any;
    public catId: number;
    public isAgreedProposal: number;
    public pinboardStatus: number;
    public isPaid: boolean;
    public title: String = "";
    public type: any;
    public isAllowPosts: any;
    public latitude: String = "";
    public longitude: String = "";
    public backgroundImage: string = "";
    public logo: string = "";
    public expiryDate: Date;
    public businessInformation: BusinessInformation;

    constructor(params: any) {
        Object.assign(this, params);
        let businessInformation = params && params.businessInformation ? params.businessInformation : {};
        this.businessInformation = businessInformation as BusinessInformation
    }


}