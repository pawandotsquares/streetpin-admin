export class User {
    public authToken: any = "";
    public email: any = "";
    public userRole: any = "";
    public userId: any = "";
    public active: any = "";
    public imageUrl: any = "";

    constructor(params: any) {
        Object.assign(this, params);
    }
}