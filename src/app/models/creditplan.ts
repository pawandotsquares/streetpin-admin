export class Creditplan {
    public planId: number;
    public planAmount: number;
    public credits: number;
    public numberOfDays: number;
    public planName: String = "";
    public planType: string = "";
    public description: string = "";

    constructor(params: any) {
        Object.assign(this, params);
    }


}