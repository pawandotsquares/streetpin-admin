export interface BusinessInformation {
     id: number;
     userId: number;
     isAgreedProposal: number;
     pinboardStatus: number;
     title: String;
     type: String;
     location: string;
     openingHours:any;
      /* openingHours:{
           mon:{
                checked: true,
                start:'',
                end:''
           },
           tue: {
                start:'',
                end:''
           },
           wed: {
                start:'',
                end:''
           },
           thu: {
                start:'',
                end:''
           },
           fri: {
                start:'',
                end:''
           },
           sat: {
                start:'',
                end:''
           },
           sun: {
                start:'',
                end:''
           }
      } */
     /*latitude: String;
     longitude: String; */
     backgroundImage: string;
     logo: string;
     about: string;
}