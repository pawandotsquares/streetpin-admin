export interface Offers {
    id: number;
    userId: number;
    offerName: String;
    title: String;
    emailId: String;
    webUrl: String;
    offerMedia: String;
    offerIcon: String;
    category: String;
    servicesHashtags: String;
    location: String;
    latitude: String;
    longitude: String;
    startDate: Date;
    expiryDate: Date;
    contactNumber: number;
    offerDescription: String;
    backgroundColor: String;
    isDeleted: number;
    pinboardList: any;
    categoryId: any;
    adminUserId: number;
    pinboardObjList: any;
}